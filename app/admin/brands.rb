ActiveAdmin.register Brand do
	
	permit_params :brandname, :desc, :tax

	menu label: "Product Brands"
	menu priority: 5


	filter :type

end
