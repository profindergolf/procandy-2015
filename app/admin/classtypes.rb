ActiveAdmin.register Classtypes do
	
	permit_params :typename, :tax, :url

	menu label: "Product Types"
	menu priority: 7


	filter :type

end
