ActiveAdmin.register Tag do
	
	permit_params :tagname, :tagtype

	menu label: "Product Tags"
	menu priority: 4


	filter :type

end
