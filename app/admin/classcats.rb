ActiveAdmin.register Classcat do
	
	permit_params :classname, :tax, :parenttype

	menu label: "Product Categories"
	menu priority: 5


	filter :type

end
