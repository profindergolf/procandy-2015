ActiveAdmin.register Profile do
	
	permit_params :name, :bio, :latlng, :stripe_id, :adcredits

	menu label: "Pro Profiles"
	menu priority: 1


	filter :name
	filter :email


end
