ActiveAdmin.register Addons do
	
	permit_params :name, :price, :featured, :highlight, :flashsale, :desc

	menu label: "Advert Addons"
	menu priority: 11

end