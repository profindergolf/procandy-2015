ActiveAdmin.register Plans do
	
	permit_params :namespace, :gold, :ad_archive, :price, :ads, :service, :photos, :homepage_ads, :local_items, :maxrange, :credits

	menu label: "Pro Account Plans"
	menu priority: 10

end