class Plans < ActiveRecord::Base

	def self.top_up_credits
		profiles = Profiles.where(["level > 2"])
		for profile in profiles
			if profile.adcredits < profile.plan.top_up_credits
				profile.adcredits =  profile.plan.top_up_credits
				profile.save
			end
		end
	end

end