class Brand < ActiveRecord::Base
  has_many :brandrel
  has_many :classifieds, through: :brandrel

  validates :brandname, uniqueness: true

end