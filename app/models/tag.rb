class Tag < ActiveRecord::Base
  has_many :classadtag
  has_many :classifieds, through: :classadtag

  validates :tagname, uniqueness: true

end