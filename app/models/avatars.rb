class Avatars < ActiveRecord::Base
	self.table_name = 'images'
	mount_uploader :theimage, AvatarUploader 
	mount_base64_uploader :theimage, AvatarUploader

	validates_presence_of :theimage
	validates_integrity_of  :theimage
  	validates_processing_of :theimage

	belongs_to :user

  def initialize(attributes={})
    attr_with_defaults = {:protype => "avatar"}.merge(attributes)
    super(attr_with_defaults)
  end
end