require 'elasticsearch/model'

class Profile < ActiveRecord::Base

	before_create :randomize_id

	paginates_per 20

	max_paginates_per 20
	
	belongs_to :user

	has_many :proserv, :dependent => :destroy
	has_many :services, through: :proserv

	has_many :profilebrand, :dependent => :destroy
	has_many :brand, through: :profilebrand

	has_one :plans, primary_key: :level, foreign_key: :id

	has_one :profilestatus

	serialize :times, JSON

	geocoded_by :postcode
	after_validation :geocode, if: ->(obj){ obj.postcode.present? and obj.postcode_changed? }

	after_save :reindex
	after_destroy :reindex

	searchkick locations: ["location"], callbacks: false, suggest: [:name, :golfclub], highlight: [:name, :golfclub, :bio], word_start: [:name, :bio, :golfclub], boost_where: {featured: {value: 1, factor: 100}}, match: :word_start, searchable: [:name, :bio, :golfclub, :brand]

	def search_data
		attributes.merge(services: services.map(&:servtitle), location: {lat: self.latitude, lon: self.longitude}, brandname: brand.map(&:brandname))
	end

	def after_save()
		Profile.reindex
    	return id = self.id
  	end

    private
      def randomize_id
        begin
          self.id = SecureRandom.random_number(1_000_000)
        end while Profile.where(id: self.id).exists?
      end	
	
	#Profile.reindex

end