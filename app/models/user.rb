class User < ActiveRecord::Base

    before_create :randomize_id

  	# Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

    has_one :avatars

    has_one :profile, dependent: :destroy
    before_validation :random_password, :on => :create
    after_create :build_profile
    #before_destroy :destroy_profile

    def random_password
	  	generated_password = Devise.friendly_token.first(8)

  		self.password = generated_password
  		self.password_confirmation = generated_password

  		Transactionals.new_pro_email(self.email, generated_password).deliver_now
    end

    def build_profile
    	profile = Profile.new
    	profile.user_id = self.id
    	profile.save
    end

    def avatars
      Avatars.where("user_id = ? AND protype = ?", self.id, 'avatar').take
    end

    def password_required?
	     new_record? ? false : super
	  end

    private
      def randomize_id
        begin
          self.id = SecureRandom.random_number(1_000_000)
        end while User.where(id: self.id).exists?
      end
end