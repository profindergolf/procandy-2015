class Classcat < ActiveRecord::Base
  has_many :classifieds

  validates :classname, uniqueness: true

end