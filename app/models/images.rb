class Images < ActiveRecord::Base
	mount_uploader :theimage, ProimagesUploader
	mount_base64_uploader :theimage, ProimagesUploader

	validates_presence_of :theimage
	validates_integrity_of  :theimage
  	validates_processing_of :theimage

	belongs_to :user
	belongs_to :classifieds
end