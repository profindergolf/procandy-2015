
class Classifieds < ActiveRecord::Base

	before_create :randomize_id

	paginates_per 20

	max_paginates_per 20
	
	belongs_to :user

	has_many :classadtag, :dependent => :destroy
	has_many :tag, through: :classadtag

	has_many :brandrel, :dependent => :destroy
	has_many :brand, through: :brandrel

	has_many :advertaddon, :dependent => :destroy
	has_many :addons, through: :advertaddon

	belongs_to :images, :foreign_key => :image, :dependent => :destroy

	belongs_to :classtypes, :foreign_key => :adtype
	belongs_to :classcat, :foreign_key => :cat

	has_many :analytic

	before_destroy :destroy_assocs
	before_create :set_default_enddate

	searchkick  callbacks: false, suggest: [:adtitle, :brandname], highlight: [:adtitle, :brandname, :description], word_start: [:adtitle, :tags, :description, :brandname], locations: ["location"], boost_where: {featured: {value: 1, factor: 100}}, match: :word_start, searchable: [:adtitle, :description, :tag, :classcats, :brandname]

	after_save :reindex, if: proc{|model| model.status == 1 }
	after_destroy :reindex

	def search_data
		featured = 0
		self.addons.each do |addon|
			if addon.name.downcase == 'featured'
				featured = 1
			end
		end
		attributes.merge(tags: tag.map(&:tagname), brandname: brand.map(&:brandname), category: classcat.classname, category_parent: classtypes.typename, location: {lat: user.profile.latitude, lon: user.profile.longitude}, featured: featured, profeatured: self.user.profile.featured)

	end

	def should_index?
    	status == 1
  	end

	def destroy_assocs
		self.images.destroy if self.images != nil 
		self.images.delete if self.images != nil 
		self.classadtag.delete_all  if self.classadtag != nil 
	end

	def set_default_enddate
		self.enddate = Time.now
	end

	def self.ad_date_check
		ads = self.where(["enddate < ?", 2.days.ago])
		for ad in ads
			ad.status = 0
			ad.save
			Classifieds.searchkick_index.remove(ad)
		end
		Classifieds.reindex
	end

	private
		def randomize_id
		  begin
		    self.id = SecureRandom.random_number(1_000_000)
		  end while Classifieds.where(id: self.id).exists?
		end

end

	#Classifieds.reindex
