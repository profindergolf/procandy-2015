class Progallery < ActiveRecord::Base
	self.table_name = 'images'
	mount_uploader :theimage, ProgalleryUploader
	mount_base64_uploader :theimage, ProgalleryUploader

	belongs_to :user

	after_destroy :delete_linked_file

  def delete_linked_file
    # Delete the linked file here
    self.remove_theimage!
  end

  def initialize(attributes={})
    attr_with_defaults = {:protype => "progallery"}.merge(attributes)
    super(attr_with_defaults)
  end
end