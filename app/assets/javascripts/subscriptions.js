

        $('.change_account ').click(function () {
        	var storethis = $(this)
            swal({
                title: "Account Change",
                text: "Please confirm you wish to change your account",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                cancelButtonText: "Cancel",
                closeOnConfirm: false
            }, function (isConfirm) {
            	if (isConfirm) {
	                swal({
	                	title: "Confirmed!", 
	                	text: "Changing your account now", 
	                	type: "success",
	                	timer: 2000,
	                	showConfirmButton: false
	                });
	                setTimeout(function(){
					    storethis.parents('form').submit();
					}, 2200);
            	}else{
            		Ladda.stopAll();
            	}
            });
            return false;
        });