// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

	$("#location-field").geocomplete();

	if($('#wrapper.search.search').length >0 ){

		Ladda.bind( 'button.main-btn' );

	    $("#ionrange_1").ionRangeSlider({
	        min: 1,
	        max: 150,
	        type: 'single',
	        postfix: " miles",
	        from: 1,
	        hasGrid: true
	    });

		$('.collapse-group a.btn').on('click', function(e) {
			if($('.collapse-group .main-btn').hasClass('hide')){
				$('.collapse-group .btn.hide').removeClass('hide');
			}else{
				$('.collapse-group .main-btn').addClass('hide');
			}
		    e.preventDefault();
		    var $this = $(this);
		    var $collapse = $this.closest('.collapse-group').find('.collapse');
		    $collapse.collapse('toggle');
		    $('.filter').toggle();
		});

		$('.filter').select2({
	        placeholder: "Results per page",
	        allowClear: true,
	        tags: false,
	        theme: "bootstrap",
	        width: '100%'
	    });

		$(".truncate").dotdotdot();

		$('#filter').on('change', function(){
			$('#perpage').val($(this).val());
			$('#searchForm').submit();
		});

		/*$(document).on('change', '#searchForm input', function(evt) {
	    	$('#searchForm').submit();
	    	return false;
		});

		$(document).on('submit', '#searchForm', function(evt) {
	    	$(this).submit();
	    	return false;
		});*/

		/*$('#sortby').on('change', function(){
			$('#order').val($(this).val());
			$('#searchForm').submit();
		});

		$('#location-field').on('change', function(){
			if($(this).val().length > 0){
				$('#sortby').find('.loc').removeAttr('disabled');
			}else{
				$('#sortby').find('.loc').attr('disabled', 'disabled');
			}
		});*/

		if(map){
			map.remove();
		}

		L.mapbox.accessToken = 'pk.eyJ1IjoicHJvZmluZGVyZ29sZiIsImEiOiJjaWo1dXZkejkwMDQzdzNtM3VoNzMwbndwIn0.aq6ruKU7kzzqM6pmwceZlA';
		var map = L.mapbox.map('map-one', 'mapbox.streets', {worldCopyJump: true, spiderfyOnMaxZoom:false, minZoom: 1, maxZoom: 18}).setView([38.8929,-77.0252], 10).invalidateSize(true);
			
		var jobLoc = L.layerGroup().addTo(map);
		var markerGroup = [];

		markCluster = new L.markerClusterGroup({
			iconCreateFunction: function(cluster) {
		        return L.mapbox.marker.icon({
		          	'marker-symbol': cluster.getChildCount(),
		          	'marker-color': '#0099CC',
		          	'marker-size': 'large',
		        });
	      	}
		});

		for (var key in markers) {
		  if (markers.hasOwnProperty(key)) {
		    //alert(key + " -> " + p[key]);
		    var venue = markers[key];
			var marker = L.marker(new L.LatLng(venue.lat,venue.lon), {
			    icon: L.mapbox.marker.icon({
				    'marker-symbol': 'circle',
				    'marker-size': 'large',
			        'marker-color': '#0099CC'
			    })
			});
			console.log(venue);
			marker.bindPopup('<a href="'+venue.link+'"><img src="'+venue.img+'"/><br/><strong>'+venue.title+'</strong><br/><span>'+venue.loc+'</span></a>');
			markerGroup[key] = [venue.lat,venue.lon];
			markCluster.addLayer(marker);
		  }
		}

		//var bounds = markCluster.getBounds();
		//$scope.map.fitBounds(bounds);

		var latlngbounds = new L.latLngBounds(markerGroup);
		map.fitBounds(latlngbounds);
		map.addLayer(markCluster);
	}
