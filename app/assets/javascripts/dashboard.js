// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

$('.showtooltip').tooltip();

theTour = function(){
    console.log('123')
	var tour = new Tour({
          steps: [
              {
                orphan: true,
                title: "Welcome to Pro Finder Golf",
                placement: "bottom",
                backdrop: true,
                backdropContainer: '#wrapper',
                content: "This quick tour will show you around the application and highlight the features available to you",
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              },
              {
                element: ".propanel",
                title: "Menu Bar",
                placement: "bottom",
                backdrop: true,
                backdropContainer: '#wrapper',
                content: "The Menu Bar is your main source of navigation around the Pro Panel dashboard. Related panels are grouped in dropdown menus so be sure to check them out for more functionality.",
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              },              
              {
                //orphan: true,
                element: "#tour-dash",
                title: "Dashboard",
                placement: "top",
                backdrop: true,
                backdropContainer: '#wrapper',
                content: "This is your dashboard. Analytic data will be represented here to help you find out what customers are looking at on your profile and how successful your adverts are working.",
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              },
              {
                orphan: true,
                title: "Your Profile",
                placement: "bottom",
                backdrop: true,
                backdropContainer: '#wrapper',
                content: "Here, you can preview and edit your profile data, as well as post a twitter-like status update to let vistors to your profile know any latest news you want to share. You can also upload additional photos to your profile from the option in the dropdown menu.",
                path: '/my-profile?tour=true',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              },
              {
                orphan: true,
                title: "Your Adverts",
                placement: "bottom",
                backdrop: true,
                backdropContainer: '#wrapper',
                content: "The Adverts (or Classifieds) section is where you create and manage all your product adverts. You can also view summaries (in the form of invoices) of your successfully sold items through Pro Finder Golf.",
                path: '/classifieds?tour=true',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              },
              {
                orphan: true,
                title: "Your Account",
                placement: "top",
                backdrop: true,
                backdropContainer: '#wrapper',
                path: '/users/edit/?tour=true',
                content: "You can find a summary of your Pro Finder Golf account here, including details of your subscription package and the ability to upgrade your package. If you have a package which allows online payments, you can check out your Stripe Merchant account here too.",
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              },
              {
                element: ".propanel",
                title: "Help",
                placement: "bottom",
                backdrop: true,
                backdropContainer: '#wrapper',
                content: "We are always available via our email help desk. You can send us a help request at anytime and we will respond to you as soon as possible. Our main opening times are between 9am-5pm GMT but we can be contacted outside that time frame. We also have a set of Frequently Asked Questions which may answer any question you have and can be viewied 24/7.",
                path: '/dashboard?tour=true',
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              },
              {
                orphan: true,
                title: "Thank You",
                placement: "bottom",
                backdrop: true,
                backdropContainer: '#wrapper',
                content: "This ends the Pro Panel dashboard tour. Thank you once again for signing up to Pro Finder Golf. Please click the 'End Tour' button to finish the tour.",
                onShown: function (tour){
                    $('body').addClass('tour-open')
                },
                onHidden: function (tour){
                    $('body').removeClass('tour-close')
                }
              }
          ],
          template: "<div class='popover tour'><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'>« Prev</button><span data-role='separator'>&nbsp;&nbsp;</span><button class='btn btn-default' data-role='next'>Next »</button><button class='btn btn-default' data-role='end'>End tour</button></div></nav></div>"
        });
    //tour.restart();
    tour.init();
    tour.start();
}