// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

	var parent = $('#proContact');
    $('.contact-dropdown').select2({
        placeholder: "Please select...",
        allowClear: true,
        tags: false,
        dropdownParent: parent,
        theme: "bootstrap",
        width: '100%'
    });

     $('#contactSubmit').on('click', function(event){
    	event.preventDefault();
    	var form = $('#pro_contact_form');
      	var validator = form.validate();
      	if(validator.form()){
      		var proid = $('#proid').val();
		    var formdata = form.serializeArray();
		    form.find('#loading').hide().removeClass('hide').fadeIn();
		    $.ajax('/profiles/'+proid+'/contact', {
		      type: 'POST',
		      data: formdata,
		      error: function(jqXHR, textStatus, errorThrown) {
		        $('.loading').hide().addClass('hide');
		        $.ladda('stopAll')
		        $('#proContact').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, we couldn\'t send your contact message. Please try again later.</div>' );
		        console.log("AJAX Error: " + textStatus);
		      },
		      success: function(data, textStatus, jqXHR) {
		        if(data.success != 'false'){              
		          $('#page-wrapper .wrapper-content').prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success</strong> Thanks for reaching out. The user will respond back to you as soon as possible.</div>' ); 
		          $('.loading').hide().addClass('hide');
		          $.ladda('stopAll')
		          $('#proContact').modal('hide');
		        }else{
		          $('.loading').hide().addClass('hide');
		          $.ladda('stopAll')
		          $('#proContact').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, we couldn\'t send your contact message. Please try again later.</div>' );
		          console.log("AJAX Error: " + textStatus);                
		        }
		      }
		    });
		}
	    return false;
    });

	    $('#profile_slick').slick({
	        dots: true,
	        arrows: false
	    });

		var $filename = false

		$('#imageUpload').on('shown.bs.modal', function () {
		  var $image = $("#image");
		  var $imageData = false;

		  $image.cropper({
		      aspectRatio: 16/9,
		      preview: ".img-preview",
		      responsive: true,
		      viewMode: 0
		  });

		  var $inputImage = $("#gallery_upload_image");
		  //var $inputImage = $('#inputImage');
		  var URL = window.URL || window.webkitURL;
		  var blobURL;

		  if (URL) {
		    $inputImage.change(function () {
		      var files = this.files;
		      var file;

		      if (!$image.data('cropper')) {
		        return;
		      }

		      if (files && files.length) {
		        file = files[0];
		        $filename = file.name;

		        if (/^image\/\w+$/.test(file.type)) {
		          blobURL = URL.createObjectURL(file);
		          $image.one('built.cropper', function () {

		            // Revoke when load complete
		            URL.revokeObjectURL(blobURL);
		          }).cropper('reset').cropper('replace', blobURL);
		          $inputImage.val('');
		        } else {
		          window.alert('Please choose an image file.');
		        }
		      }
		    });
		  } else {
		    $inputImage.prop('disabled', true).parent().addClass('disabled');
		  }

		
		  $("#zoomIn").click(function () {
		      $image.cropper("zoom", 0.1);
		  });

		  $("#zoomOut").click(function () {
		      $image.cropper("zoom", -0.1);
		  });

		  $("#rotateLeft").click(function () {
		      $image.cropper("rotate", 45);
		  });

		  $("#rotateRight").click(function () {
		      $image.cropper("rotate", -45);
		  });

		  $("#setDrag").click(function () {
		      $image.cropper("setDragMode", "crop");
		  });

		  if(!$('#uploadButton').hasClass('disabled')){
			$('#uploadButton').on('click', function(e){
				e.preventDefault();
				var id = $(this).data('id');
				$('#uploadButton').addClass('disabled');
				$('#loadingImg').hide().removeClass('hide').fadeIn();
				$image.cropper('getCroppedCanvas').toBlob(function (blob) {
				  	var formData = new FormData();
				  	formData.append('croppedImage', blob, $filename);
					$.ajax('/profile/'+id+'/gallery/new', {
				        type: 'POST',
					    processData: false,
					    contentType: false,
				        data: formData,
			            error: function(jqXHR, textStatus, errorThrown) {
			              $('#uploadButton').removeClass('disabled');
			              $('#loadingImg').hide().addClass('hide');
			              $('#imageUpload').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, there has been an error uploading your image.</div>' );
			              console.log("AJAX Error: " + textStatus);
			            },
			            success: function(data, textStatus, jqXHR) {
			              $('#uploadButton').removeClass('disabled');
			              $('#page-wrapper .wrapper-content').prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success</strong> Your image has been uploaded successfully</div>' ); 
			              $('#loadingImg').hide().addClass('hide');
			              $('#imageUpload').modal('hide');
			              location.reload(); 
			            }
			    	});
			    });
			    return false;
			});
		   }
		});

		$('#imageUpload').on('hidden.bs.modal', function () {
			var $image = $("#image");
			$('#uploadButton').removeClass('disabled');
			$image.cropper("destroy");
		});

		$('.file .delete').on('click', function(){
			var obj = $(this)
			obj.parents('.file-box').css('opacity', '0.5');
			var userid = $(this).data('userid');
			var imgid = $(this).data('imgid');
			$.ajax('/profile/'+userid+'/gallery/'+imgid+'/delete', {
		        type: 'GET',
			    processData: false,
			    contentType: false,
	            error: function(jqXHR, textStatus, errorThrown) {
	              obj.parents('.file-box').css('opacity', '1');
	              $('#imageUpload').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, there has been an error deleting your image.</div>' );
	              console.log("AJAX Error: " + textStatus);
	            },
	            success: function(data, textStatus, jqXHR) {
	              obj.parents('.file-box').remove();
	              $('#page-wrapper .wrapper-content').prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success</strong> Your image has been deleted successfully</div>' ); 
	            }
	    	});
		});	
		        L.mapbox.accessToken = 'pk.eyJ1IjoicHJvZmluZGVyZ29sZiIsImEiOiJjaWo1dXZkejkwMDQzdzNtM3VoNzMwbndwIn0.aq6ruKU7kzzqM6pmwceZlA';

		if($('#showClub').length > 0){
			if(!map){
		        var $geocoder = L.mapbox.geocoder('mapbox.places'),
		            $map = L.mapbox.map('showClub', 'examples.map-h67hf2ic');

		          L.mapbox.featureLayer({
		              type: 'Feature',
		              geometry: {
		                  type: 'Point',
		                  coordinates: [
		                    proLon,
		                    proLat
		                  ]
		              },
		              properties: {
		                  title: '<%= @profile.golfclub %>',
		                  'marker-size': 'large',
		                  'marker-color': '#228844',
		                  'marker-symbol': 'golf'
		              }
		          }).addTo($map);

		          $map.setView([proLat, proLon], 13);
		    }else{
		    	map.remove();
		     	var $geocoder = L.mapbox.geocoder('mapbox.places'),
		        $map = L.mapbox.map('showClub', 'examples.map-h67hf2ic');
		          L.mapbox.featureLayer({
		              type: 'Feature',
		              geometry: {
		                  type: 'Point',
		                  coordinates: [
		                    proLon,
		                    proLat
		                  ]
		              },
		              properties: {
		                  title: '<%= @profile.golfclub %>',
		                  'marker-size': 'large',
		                  'marker-color': '#228844',
		                  'marker-symbol': 'golf'
		              }
		          }).addTo($map);

		          $map.setView([proLat, proLon], 13);
		    }
      	}

      	var $newfile = false;

		$("#profile-wizard").steps({
		      bodyTag: "fieldset",
		      onContentLoaded: function (event, currentIndex){

		      },
		      onStepChanging: function (event, currentIndex, newIndex) {
		          // Always allow going backward even if the current step contains invalid fields!
		          if (currentIndex > newIndex) {
		              return true;
		          }

		          var form = $(this);

		          // Clean up if user went backward before
		          if (currentIndex < newIndex) {
		              // To remove error styles
		              $(".body:eq(" + newIndex + ") label.error", form).remove();
		              $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
		          }

		          // Disable validation on fields that are disabled or hidden.
		          form.validate().settings.ignore = ":disabled,:hidden";

		          // Start validation; Prevent going forward if false
		          return form.valid();
		      },
		      onStepChanged: function (event, currentIndex, priorIndex) {

		          if (currentIndex === 0){
		          }

		          if (currentIndex === 1){

					var $image = $("#image")

					  $image.cropper({
					      aspectRatio: 16/9,
					      preview: ".img-preview",
					      done: function (data) {
					      	  $newfile = true
					          $('#image-url').val($image.cropper('getCroppedCanvas').toDataURL('image/jpeg'));
					      }
					  });

					  var $inputImage = $("#profiles_theimage");
					  if (window.FileReader) {
					      $inputImage.change(function () {
					      	  $newfile = true
					          var fileReader = new FileReader(),
					                  files = this.files,
					                  file;

					          if (!files.length) {
					              return;
					          }

					          file = files[0];

					          if (/^image\/\w+$/.test(file.type)) {
					              fileReader.readAsDataURL(file);
					              fileReader.onload = function () {
					                  $inputImage.val("");
					                  $image.cropper("reset", true).cropper("replace", this.result);
					              };
					          } else {
					              showMessage("Please choose an image file.");
					          }
					      });
					  } else {
					      $inputImage.addClass("hide");
					  }

					
					  $("#zoomIn").click(function () {
					      $image.cropper("zoom", 0.1);
					  });

					  $("#zoomOut").click(function () {
					      $image.cropper("zoom", -0.1);
					  });

					  $("#rotateLeft").click(function () {
					      $image.cropper("rotate", 45);
					  });

					  $("#rotateRight").click(function () {
					      $image.cropper("rotate", -45);
					  });

					  $("#setDrag").click(function () {
					      $image.cropper("setDragMode", "crop");
					  });
					}else if(currentIndex === 2){
						$('.summernote').summernote({ 
						    toolbar: [
						      ['style', ['bold', 'italic', 'underline', 'clear']],
						      ['para', ['ul', 'ol']],
						      ['extra', ['fullscreen', 'undo', 'redo', 'help']],
						    ] 
						  });
						if(!map){
							L.mapbox.accessToken = 'pk.eyJ1IjoicHJvZmluZGVyZ29sZiIsImEiOiJjaWo1dXZkejkwMDQzdzNtM3VoNzMwbndwIn0.aq6ruKU7kzzqM6pmwceZlA';
							var $geocoder = L.mapbox.geocoder('mapbox.places'),
							    $map = L.mapbox.map('postcode', 'examples.map-h67hf2ic');

							var typingTimer;  
							var doneTypingInterval = 2000;
							var $input = $('#profile_postcode');

							$input.on('keyup', function(){
							  	clearTimeout(typingTimer);
							  	var val = $(this).val()
	  							typingTimer = setTimeout(function(){$geocoder.query(val, showMap);}, doneTypingInterval);
							});

							$input.on('keydown', function () {
							  clearTimeout(typingTimer);
							});

							function showMap(err, data) {

								L.mapbox.featureLayer({
								    type: 'Feature',
								    geometry: {
								        type: 'Point',
								        coordinates: [
								          data.latlng[1],
								          data.latlng[0]
								        ]
								    },
								    properties: {
								        title: 'Your Location',
								        'marker-size': 'large',
								        'marker-color': '#228844',
								        'marker-symbol': 'golf'
								    }
								}).addTo($map);

							    if (data.lbounds) {
							        $map.fitBounds(data.lbounds);
							    } else if (data.latlng) {
							        $map.setView([data.latlng[0], data.latlng[1]], 13);
							    }
							}


							if( $input.val().length > 0 ) {
								$geocoder.query($input.val(), showMap);
							}
						}

					}else if(currentIndex === 4){			
						$("#brands").select2({
					        placeholder: "Select your suppliers...",
					        allowClear: true,
					        tags: true,
					        theme: "bootstrap",
					        width: '100%',
					        createTag: function(params) {
					          return undefined;
					        },
					        maximumSelectionLength: 10,
					        formatSelectionTooBig: function (limit) {
					            return 'Too many selected items';
					        }
					    });
					}

		      },
		      onFinishing: function (event, currentIndex) {
		          var form = $(this);

		          	if($newfile){
						var $image = $("#image")
						$('#image-url').val($image.cropper('getCroppedCanvas').toDataURL('image/jpeg'));
					}

		          // Disable validation on fields that are disabled.
		          // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
		          form.validate().settings.ignore = ":disabled";

		          // Start validation; Prevent form submission if false
		          return form.valid();
		      },
		      onCanceled: function(){
		      	window.location = '/my-profile';
		      },
		      onFinished: function (event, currentIndex) {
		          var form = $(this);

		          // Submit form input
		          form.submit();
		      }
		  }).validate({
		      errorPlacement: function (error, element) {
		          element.before(error);
		      },
		      rules: {
		          confirm: {
		              equalTo: "#password"
		          }
		      }
		  });
	
		$('#theServices input').on('change', function(evt) {
			if(parseInt(document.querySelectorAll('input[type="checkbox"]:checked').length)-1 > serviceLimit){
				$(this).prop("checked", false);
				swal(
				  {   
					title: "Services Limit Reached",   
					text: "You have reached the maximum number of services you can select. For more, please upgrade your subscription level.",   
					type: "warning",   
  					timer: 3000,   
  					showConfirmButton: false
			    });
				return false;
			}
		 });

		  $('.dropdown').select2({
		      placeholder: "Please select...",
		      allowClear: true,
		      tags: false,
		      theme: "bootstrap",
		      width: '100%'
		  });


	$('.clockpicker').clockpicker({
		donetext: "Set Time"
	});

  $(document).on('click', '#deleteStatus', function(e){
	var obj = $(this)
	swal(
	  {   
		title: "Confirm Deletion?",   
		text: "Your status will be permanently deleted",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false 
      }, function(){  
      	var id = obj.data('id')
		$.ajax('/profile/status/'+id, {
	        type: 'DELETE',
		    processData: false,
		    contentType: false,
            error: function(jqXHR, textStatus, errorThrown) {
              swal("Error, Sorry!", "Sorry, there was an problem deleting your status. Please try again.", "error");
              console.log("AJAX Error: " + textStatus);
            },
            success: function(data, textStatus, jqXHR) {
              swal("Deleted Successfully!", "Your status has been deleted", "success");
              obj.parent().remove();
            }
    	});
    	return false;    	
      });
	return false;
});

$('#statusUpdate').on('shown.bs.modal', function () {
	function updateCountdown() {
	    // 140 is the max message length
	    var remaining = 140 - $('#statusUpdate .message').val().length;
	    $('#statusUpdate .countdown').text(remaining + ' characters remaining.');
	    if(remaining < 0){
	    	$('#postStatus').addClass('disabled').attr('disabled', 'disabled');
	    }else{
	    	$('#postStatus').removeClass('disabled').removeAttr('disabled', 'disabled');
	    }
	}

    updateCountdown();
    $('#statusUpdate .message').change(updateCountdown);
    $('#statusUpdate .message').keyup(updateCountdown);

	if(!$('#postStatus').hasClass('disabled')){
		$('#postStatus').on('click', function(e){
			var message = $('#statusUpdate .message').val()
			var obj = $(this)
			var userid = $(this).data('id');
			$.ajax('/profile/'+userid+'/status', {
		        type: 'POST',
		        data: {
		        	status: message
		        },
	            error: function(jqXHR, textStatus, errorThrown) {
	              $('#imageUpload').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, there has been an error posting your status update.</div>' );
	              console.log("AJAX Error: " + textStatus);
	              $('#imageUpload').modal('hide');
	            },
	            success: function(data, textStatus, jqXHR) {
	              var statusData = JSON.parse(data.data)
	              $('#page-wrapper .wrapper-content').prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success</strong> Your status update has been posted successfully</div>' ); 
	              $('#status-list .status').remove();
	              $('#status-list').append('<div class="status" id="status-'+statusData.id+'"><p  class="col-sm-10">'+message+'</p><a href="#" id="deleteStatus" data-id="'+statusData.id+'" onclick="return false" class="col-sm-1 pull-right btn btn-danger danger"><i class="fa fa-trash"></i></a></div>');
	              $('#statusUpdate .message').val(' ')
	              $('#statusUpdate').modal('hide');
	            }
	    	});

	    	return false;
		});
	}
});