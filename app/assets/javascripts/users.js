	$('#newcardmodal').on('shown.bs.modal', function () {
	    $('#card').card({
	        container: '.card-wrapper'
	    });

		$('#update_card_form').on('submit', function(e){
			e.preventDefault();
			var form = $(this);
		    var validator = form.validate();
		    if(validator.form()){
		    	var expiry = $('#expiry').val().split(' / ');

	            Stripe.card.createToken({
	                number: $('#num').val(),
	                cvc: $('#cvc').val(),
	                exp_month: expiry[0],
	                exp_year: expiry[1],
	                name: $('#name').val()
	            }, updateCard);
		    }
		    return false
		});

		updateCard = function(status, response){
		  	var $form = $('#update_card_form');

	      	if (response.error) {
	        	$(document).find('#newcardmodal').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> '+response.error.message+'</div>' );
	        	$.ladda('stopAll');
	      	} else {
		        var token = response.id;
				$.ajax('/users/'+userid+'/subscribe/cardchange', {
		          type: 'POST',
		          data: {
		          	token: token,
		            stripe_id: stripe_id
		          },
		          error: function(jqXHR, textStatus, errorThrown) {
		            $.ladda('stopAll');
		            $(document).find('#newcardmodal').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> There has been an error processing your card. Please try again later.</div>' );
		          },
		          success: function(data, textStatus, jqXHR) {
		            $.ladda('stopAll');
		           	location.reload()
		           	$('#newcardmodal').modal('hide');
		          }
		    	});
		    }
		}
	});