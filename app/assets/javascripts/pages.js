	$('.anystretch').anystretch();

	loadBlogPosts = function(limit){
		var target = $('#thePosts')
		$.ajax('getBlogPosts', {
	      type: 'POST',
	      dataType: "html",
	      data: {
	      	postLimit: limit
	      },
	      error: function(jqXHR, textStatus, errorThrown) {
	      	target.find('.loading').fadeOut().remove();
	      },
	      success: function(data, textStatus, jqXHR) {
	        target.find('.loading').fadeOut().remove();
	        target.html(data).find('.postsData').fadeIn();
	      }
	    });
	}
    
    var video = document.getElementsByTagName('video');
    for (var i = 0; i < video.length; i++) { 
    	console.log(video[i])
    	video[i].addEventListener('ended',myHandler,false);
    }

    function myHandler(e) {
        console.log(e)
    }

	if ($('#thePosts').length){
		loadBlogPosts(3);
	}

	$('#sponsor-slide').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 6000,
        arrows: false
    });

    var usersubmit = $("#usersubmit_form");

     $('.sign-up-form').on('click', function(){
     	console.log($(this).data('plan'));
    	usersubmit.find('#plan').val($(this).data('plan'));
    });

	  usersubmit.on('submit', function(e){
	    var $form = $(this)
	    var validator = $form.validate();
	    if(validator.form()){
	      var formdata = $(this).serializeArray();
	      $(this).find('#loading').hide().removeClass('hide').fadeIn();
	      $.ajax('https://script.google.com/macros/s/AKfycbwWGtibYCjTa5BdytHyO6pkDxBsjQotfr-QjVC1dPrHlMtGmLI/exec', {
	        type: 'POST',
	        data: formdata,
	        error: function(jqXHR, textStatus, errorThrown) {
	          $.ladda('stopAll');
	          $('#loading').hide().addClass('hide');
	          $('#regModal').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, there has been an error submitting your sign up request. Please try again later.</div>' );
	          console.log("AJAX Error: " + textStatus);
	        },
	        success: function(data, textStatus, jqXHR) {
	          $.ladda('stopAll');
	          $('.loading').hide().addClass('hide');
	          $('#regModal').modal('hide');
          	  swal("Feedback Submitted", "Thanks so much, we'll get your registered as soon as possible!", "success")
	        }
	      });
	    }
	    return false;
	  });
