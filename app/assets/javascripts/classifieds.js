
  var credForm = $("#adcredit_form");

  var dateStart = '';
  var dateEnd = '';

  $('.product-images').slick({
      dots: true
  });

  credForm.on('submit', function(e){
    var formdata = $(this).serializeArray();
    formdata[dateStart] = dateStart;
    formdata[dateEnd] = dateEnd;
    $(this).find('#loading').hide().removeClass('hide').fadeIn();
    $.ajax('addonCreditsProcess', {
      type: 'POST',
      data: formdata,
      error: function(jqXHR, textStatus, errorThrown) {
        $(this).find('#loading').addClass('hide');
        $(this).parents('body').find('#creditsModal').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong>'+data.notice+'</div>' );
        console.log("AJAX Error: " + textStatus);
      },
      success: function(data, textStatus, jqXHR) {
        if(data.success != 'false'){              
          $('#page-wrapper .container').prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success</strong> Your purchase has been completed sucessfully! Thank you</div>' ); 
          $('#loading').hide().addClass('hide');
          $('#creditsModal').modal('hide');
        }else{
          $('#loading').hide().addClass('hide');
          $('#creditsModal').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong>'+data.notice+'</div>' );
          console.log("AJAX Error: " + textStatus);                
        }
      }
    });
    return false;
  });

  removeAddon = function(addId){
    $('#addon-'+addId).fadeOut().remove();
    $('#addonc-'+addId).fadeOut().remove();
    var array = $('#addonids1').val();
    var $split = array.split(',');
    for(var i=0; i<$split.length; i++) { 
      var check = $split[i] = +$split[i].replace(/[^\w\s]/gi, '').replace(/\s+/g, '');  
      if(check == +addId){
        $split.splice(i, 1);
      }
    }
    $('#addonids1').val($split);
    $('#addonids2').val($split);
    $('#addoncids').val($split);
    return false;
  };

  stripePay = function(jsondata){
    handler.open({
      name: 'Pro Finder Golf',
      description: jsondata.description,
      currency: "gbp",
      email: jsondata.email,
      amount: jsondata.amount
    })
  };

  authStripeToken = function(token,id,addons){
    $.ajax('/classifieds/'+id+'/addons', {
      type: 'POST',
      data: {
        stripeToken: token.id, 
        enddate: dateEnd,
        aid: addons
      },
      error: function(jqXHR, textStatus, errorThrown) {
        $.ladda('stopAll');
        return console.log("AJAX Error: " + textStatus);
      },
      success: function(data, textStatus, jqXHR) {
        $.ladda('stopAll');
        return data;
      }
    });
  }

  authProductStripeToken = function(token,id,addons){
    $.ajax('/classifieds/payment/confirmation/'+id, {
      type: 'POST',
      data: {
        stripeToken: token.id
      },
      error: function(jqXHR, textStatus, errorThrown) {
        $.ladda('stopAll');
        return console.log("AJAX Error: " + textStatus);
      },
      success: function(data, textStatus, jqXHR) {
        $.ladda('stopAll');
        return data;
      }
    });
  }

  $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
  });

  $('.i-checks.ad-list-actions').on('ifChecked', function(event){
      $(this).parents('table.issue-tracker').find('.highlight').removeClass('highlight');
      $(this).parents('tr').addClass('highlight');
      var id = $(this).data('adnum');
      $('#the-actions .change-id').each(function(){
        var href = $(this).attr('href');
        var url = href.replace(/(\d+)/g, id);
        $(this).attr('href',url);
      });
      $('.changeStatsDrop').data('ad', id);
  });

  $('#addelete').on('click', function(e){
    var obj =  $(this)
    swal({   
        title: "Delete Advert?",   
        text: "This cannot be undone. Do you wish to delete this advert?",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes, delete",   
        closeOnConfirm: true
      }, function(){  
        $.ajax(obj.attr('href'), {
            type: 'DELETE',
            error: function(jqXHR, textStatus, errorThrown) {
              location.reload()
            },
            success: function(data, textStatus, jqXHR) {
              location.reload()
            }
        });
    })
    return false
  });

  var $image = '';
  var imageChanged = false;


  deleteImage = function($this){
    var obj = $($this)
    obj.parents('.file-box').css('opacity', '0.5');
    var adid = $($this).data('adid');
    var imgid = $($this).data('imgid');
    $.ajax('/classifieds/'+adid+'/images/'+imgid, {
        type: 'DELETE',
        processData: false,
        contentType: false,
        error: function(jqXHR, textStatus, errorThrown) {
          obj.parents('.file-box').css('opacity', '1');
          $('#imageUpload').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, there has been an error deleting your image.</div>' );
          console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          obj.parents('.file-box').remove();
          $('#page-wrapper .wrapper-content').prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success</strong> Your image has been deleted successfully</div>' ); 
          var fileBoxNum = $('.file-box').length;    
          if(fileBoxNum >= limitNum){
            $('.extraUpload').attr('disabled', 'disabled').addClass('disabled');
            $('#extraUploadButton').attr('disabled', 'disabled').addClass('disabled');
          }else{
            $('.extraUpload').removeAttr('disabled').removeClass('disabled');
            $('#extraUploadButton').removeAttr('disabled').removeClass('disabled');            
          }
        }
    });
    return false;
  }

  $(document).on('click', '.extra-delete-nonajax', function(e){
    $(this).parents('.file-box').remove();
    var fileBoxNum = $('.file-box').length;
    if(fileBoxNum >= limitNum){
      $('.extraUpload').attr('disabled', 'disabled').addClass('disabled');
      $('#extraUploadButton').attr('disabled', 'disabled').addClass('disabled');
    }else{
      $('.extraUpload').removeAttr('disabled').removeClass('disabled');
      $('#extraUploadButton').removeAttr('disabled').removeClass('disabled');            
    }
  });

  $('#imageUpload').on('hidden.bs.modal', function () {
    var fileBoxNum = $('.file-box').length;    
    if(fileBoxNum >= limitNum){
      $('.extraUpload').attr('disabled', 'disabled').addClass('disabled');
      $('#extraUploadButton').attr('disabled', 'disabled').addClass('disabled');
    }else{
      $('.extraUpload').removeAttr('disabled').removeClass('disabled');
      $('#extraUploadButton').removeAttr('disabled').removeClass('disabled');            
    }
  });

  var $preloadimg = false

  $("#ad-wizard").steps({
      bodyTag: "fieldset",
      onStepChanging: function (event, currentIndex, newIndex) {
          // Always allow going backward even if the current step contains invalid fields!
          if (currentIndex > newIndex) {
              return true;
          }

          var form = $(this);

          // Clean up if user went backward before
          if (currentIndex < newIndex) {
              // To remove error styles
              $(".body:eq(" + newIndex + ") label.error", form).remove();
              $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
          }

          // Disable validation on fields that are disabled or hidden.
          form.validate().settings.ignore = ":disabled,:hidden";

          // Start validation; Prevent going forward if false
          return form.valid();
      },
      onStepChanged: function (event, currentIndex, priorIndex) {
          if (currentIndex === 0){
            $('.select2dropdown').select2({
                placeholder: "Please select...",
                allowClear: true,
                tags: false,
                theme: "bootstrap",
                width: '100%'
            });
          }

          if(currentIndex === 5){

              var total = 0;
              if(dateEnd){
                var ms = dateStart.diff(dateEnd);
                var d = moment.duration(ms);
                var s = Math.abs(Math.floor(d.asDays()))-1;
              }else{
                var s = dateMaxDays
              }

              $('.feature-check').iCheck({
                  checkboxClass: 'icheckbox_square-green',
                  radioClass: 'iradio_square-green',
              }).on('ifChecked', function(e){
                total += (parseFloat(e.target.value/100))*s
                $('#total').text('£'+Math.abs(total).toFixed(2));
              }).on('ifUnchecked', function(e){
                total -= (parseFloat(e.target.value/100))*s
                $('#total').text('£'+Math.abs(total).toFixed(2));
              });
          }

          if (currentIndex === 1){
            $image = $("#image")

            $image.cropper({
                aspectRatio: 16/9,
                preview: ".img-preview-avatar",          
                responsive: true,
                viewMode: 2
            });
            if(!$preloadimg){
              var $inputImage = $("#classifieds_theimage");
              var URL = window.URL || window.webkitURL;
              var blobURL;

              if (URL) {
                $inputImage.change(function () {
                  imageChanged = true
                  var files = this.files;
                  var file;

                  if (!$image.data('cropper')) {
                    return;
                  }

                  if (files && files.length) {
                    file = files[0];
                    $filename = file.name;

                    if (/^image\/\w+$/.test(file.type)) {
                      blobURL = URL.createObjectURL(file);
                      $image.one('built.cropper', function () {

                        // Revoke when load complete
                        URL.revokeObjectURL(blobURL);
                      }).cropper('reset').cropper('replace', blobURL);
                      $inputImage.val('');
                    } else {
                      window.alert('Please choose an image file.');
                    }
                  }
                });

                if($('#image-url').val() && !imageChanged){
                  imageChanged = true
                  var externalImg = $('#image-url').val();
                  $image.cropper('replace',externalImg);
                }
              } else {
                $inputImage.prop('disabled', true).parent().addClass('disabled');
              }

            
              $("#zoomIn").click(function () {
                  $image.cropper("zoom", 0.1);
              });

              $("#zoomOut").click(function () {
                  $image.cropper("zoom", -0.1);
              });

              $("#rotateLeft").click(function () {
                  $image.cropper("rotate", 45);
              });

              $("#rotateRight").click(function () {
                  $image.cropper("rotate", -45);
              });

              $("#setDrag").click(function () {
                  $image.cropper("setDragMode", "crop");
              });

              $preloadimg = true
            }
          }
          if(currentIndex === 3){
            $('.summernote').summernote({ 
                toolbar: [
                  ['style', ['bold', 'italic', 'underline', 'clear']],
                  ['format', ['table']],
                  ['para', ['ul', 'ol']],
                  ['extra', ['fullscreen', 'undo', 'redo', 'codeview', 'help']],
                ] 
              });
          }
          if(currentIndex === 2){
            if(imageChanged){
              var data = $image.cropper('getCroppedCanvas').toDataURL('image/jpeg');
              $('#image-url').val(data);
            }
            var fileBoxNum = $('.file-box').length;

            if(fileBoxNum >= limitNum){
              $('.extraUpload').attr('disabled', 'disabled').addClass('disabled');
              $('#extraUploadButton').attr('disabled', 'disabled').addClass('disabled');
            }else{
              $('.extraUpload').removeAttr('disabled').removeClass('disabled');
              $('#extraUploadButton').removeAttr('disabled').removeClass('disabled');            
            }

            var $imageExtra = $("#extraimage");
            var $imageData = false;

            $imageExtra.cropper({
                aspectRatio: 16/9,
                preview: ".img-preview-extra",
                responsive: true,
                viewMode: 2
            });

            var $inputImage = $("#gallery_upload_image");
            //var $inputImage = $('#inputImage');
            var URL = window.URL || window.webkitURL;
            var blobURL;

            if (URL) {
              $inputImage.change(function () {
                var files = this.files;
                var file;

                if (!$imageExtra.data('cropper')) {
                  return;
                }

                if (files && files.length) {
                  file = files[0];
                  $filename = file.name;

                  if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $imageExtra.one('built.cropper', function () {

                      // Revoke when load complete
                      URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                  } else {
                    window.alert('Please choose an image file.');
                  }
                }
              });
            } else {
              $inputImage.prop('disabled', true).parent().addClass('disabled');
            }

          
            $("#exzoomIn").click(function () {
                $imageExtra.cropper("zoom", 0.1);
            });

            $("#exzoomOut").click(function () {
                $imageExtra.cropper("zoom", -0.1);
            });

            $("#exrotateLeft").click(function () {
                $imageExtra.cropper("rotate", 45);
            });

            $("#exrotateRight").click(function () {
                $imageExtra.cropper("rotate", -45);
            });

            var extraimgprocess = false

            if(!$('#extraUploadButton').hasClass('disabled')){
              $('#extraUploadButton').on('click', function(e){
                e.preventDefault();
                if(!extraimgprocess){
                  extraimgprocess = true
                  var id = $(this).data('id');
                  $('#extraUploadButton').addClass('disabled');
                  Ladda.stopAll();
                  var data = $imageExtra.cropper('getCroppedCanvas').toDataURL('image/jpeg');
                  $('#theExtraImages').append('<div class="file-box col-lg-4"><div class="file"><div class="actions"><a class="btn btn-primary extra-delete-nonajax"><i class="fa fa-trash"></i></a></div><span class="corner"></span><div class="icon"><img alt="image" class="img-responsive" src="'+data+'"></div></div><input type="hidden" name="classifieds[extraImage][][data]" value="'+data+'" /></div>')
                  $('#theExtraImages > h3').remove();
                  $('#extraUploadButton').removeClass('disabled');
                  $('#imageUpload').modal('hide');
                  extraimgprocess = false
                }
                return false;
              });
            }

            $('#imageUpload').on('hidden.bs.modal', function () {
              var $imageExtra = $("#extraimage");
              $('#extraUploadButton').removeClass('disabled');
              $('body').removeClass('modal-open');
              $('.modal-backdrop').remove();
              $imageExtra.cropper('replace', placeholder);
            });
          }
      },
      onFinishing: function (event, currentIndex) {
          var form = $(this);

          // Disable validation on fields that are disabled.
          // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
          form.validate().settings.ignore = ":disabled";

          // Start validation; Prevent form submission if false
          return form.valid();
      },
      onFinished: function (event, currentIndex) {
          var form = $(this);

          // Submit form input
          form.submit();
      }
  }).validate({
      errorPlacement: function (error, element) {
          element.before(error);
      },
      rules: {
          confirm: {
              equalTo: "#password"
          }
      }
  });


  $('#adselecttype').on('change', function(){
    var value = $(this).val()
    console.log(value)
    if(value != 2){
      $('.hideMe .input-group-btn').hide();
      $('.hideMe .changeInput').addClass('form-group').removeClass('input-group');
    }else{
      $('.hideMe .input-group-btn').show();
      $('.hideMe .changeInput').addClass('input-group').removeClass('form-group')
    }
  });

  $(document).on('click', '#issueRefund', function(e){
    var obj = $(this)
    swal(
      {   
        title: "Confirm Refund?",   
        text: "This cannot be undone. Do you wish to process a refund?",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes, process refund",   
        closeOnConfirm: false 
      }, function(){  
          var id = obj.data('id')
          $.ajax('/invoices/refund/'+id, {
            type: 'PUT',
            processData: false,
            contentType: false,
            error: function(jqXHR, textStatus, errorThrown) {
                swal("Error, Sorry!", "Sorry, there was an problem processing the refund. Please try again.", "error");
                console.log("AJAX Error: " + textStatus);
            },
              success: function(data, textStatus, jqXHR) {
                swal("Refund Sumitted!", "The refund has been sumitted and will be processed shortly.", "success");
                obj.parent().remove();
            }
          });
          return false;     
    });
    return false;
  });

  $('.select2dropdown').select2({
      placeholder: "Please select...",
      allowClear: true,
      tags: false,
      theme: "bootstrap",
      width: '100%'
  });

  $( "#classTags" ).select2({
      allowClear: false,
      tags: true,
      theme: "bootstrap",
      placeholder: "Please select...",
      width: '100%',
      createTag: function(params) {
        return undefined;
      }
  });

  var productVal = 0;

  $('#adselecttype').select2({
      placeholder: "Please select...",
      allowClear: true,
      tags: false,
      theme: "bootstrap",
      width: '100%'
    }).on("change", function(e) { 
      productVal = $(this).val();
      if($(this).val() > 1){
        $('#classifieds_url').parents('.hideMe').removeClass('hidden');
      }else{
        $('#classifieds_url').parents('.hideMe').addClass('hidden');
      }
  });

  /*$('.changeStatsDrop').on('click', function(){
    var num = $(this).attr('data-num')
    $('#hideStats'+num).removeClass('hidden')
  });*/


  $('#apiGo').on('click', function(){
    var l = $(this).ladda();
    l.ladda('start');
    var str = $('#classifieds_url').val();
    if(productVal == 2){
      var end = str.length;
      if(str.lastIndexOf("?") > -1){
        end = str.lastIndexOf("?") 
      }
      var id  = str.substring(str.lastIndexOf("/")+1,end)
      return $.ajax('/ebaygetitem', {
        type: 'GET',
        dataType: 'json',
        data: {
          ebayid: id
        },
        error: function(jqXHR, textStatus, errorThrown) {
          l.ladda('stop');
          $("#alert-append").prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> We could not load the product information from eBay. Please check your url and try again later.</div>' );
          //return console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          l.ladda('stop');
          $('#externalId').val(id)
          $('#classifieds_adtitle').val(data.Item.Title)
          $('#classifieds_description').val(data.Item.Description)
          $('#classifieds_price').val(data.Item.ConvertedCurrentPrice.Value)
          $('#image-url').val(data.Item.PictureURL[0])
          $('#remoteImage').attr('src', data.Item.PictureURL[0])
          $('#remote').removeClass('hidden');
          $("#alert-append").prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Sucess</strong> Details of your product have been collected from eBay.</div>' );
        }
      });
    }else{
      return $.ajax('/gbgetitem', {
        type: 'GET',
        dataType: 'json',
        data: {
          ebayid: str
        },
        error: function(jqXHR, textStatus, errorThrown) {
          l.ladda('stop');
          $("#alert-append").prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> We could not load the product information from Golf Bidder. Please check your url and try again later.</div>' );
          //return console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          l.ladda('stop');
          console.log(data);
          $('#classifieds_adtitle').val(data.results[0].adtitle)
          $('#classifieds_description').val(data.results[0].desc)
          $('#image-url').val(data.results[0].productpic)
          $('#classifieds_price').val(data.results[0].price)
          $("#alert-append").prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Sucess</strong> Details of your product have been collected from Golf Bidder.</div>' );
        }
      });      
    }
  });

  $('#archiveChange').on('click', function(){
    var adid = parseInt($(this).data('ad'))
    var status = 0;
    return $.ajax('/changeArchive', {
        type: 'GET',
        dataType: 'json',
        data: {
          adid: adid,
          status: status
        },
        error: function(jqXHR, textStatus, errorThrown) {
          return console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          document.location.reload(true);
        }
    }); 
  });

  $('#reportrange.allowRange').daterangepicker({
      startDate: moment(),
      minDate: moment(),
      format: 'DD/MM/YYYY',
      dateLimit: { days: $('#reportrange').data('range') },
      showDropdowns: true,
      showWeekNumbers: true,
      separator: ' to '
  }, function(start, end, label) {
      dateStart = start;
      dateEnd = end;
      $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
      $('#enddate').val(end.format('YYYY-MM-DD 00:00:00'));
  });

  $('#detailsModal').on('shown.bs.modal', function () {
    $('#card').card({
        container: '.card-wrapper',
    });

    $('#purchase_form').submit(function(event) {
      var $form = $(this);
      var validator = $form.validate();
      if(validator.form()){

        if(!$('#carddetails').parent().hasClass('active')){
          $('#carddetails').tab('show');
        }else{
          $form.find('#helpSubmit').prop('disabled', true);

          var expiry = $('#expiry').val().split(' / ');

          Stripe.card.createToken({
                number: $('#num').val(),
                cvc: $('#cvc').val(),
                exp_month: expiry[0],
                exp_year: expiry[1],
                name: $('#name').val(),
                address_line1:$('#street1').val(),
                address_line2:$('#street2').val(),
                address_city:$('#city').val(),
                address_zip:$('#postcode').val(),
                address_state:$('#county').val(),
                address_country:$('#user_country').val()
          }, stripeResponseHandler);
        }
      }
      return false;
    });

    function stripeResponseHandler(status, response) {
      var $form = $('#purchase_form');

      if (response.error) {
        // Show the errors on the form
        $form.find('.payment-errors').text(response.error.message);
        $form.find('button').prop('disabled', false);
      } else {
        // response contains id and card, which contains additional card details
        var token = response.id;
        var email = $form.find('#email').val();

        // Insert the token into the form so it gets submitted to the server
        $.ajax('/classifieds/payment/confirmation/'+$form.find('#adid').val(), {
          type: 'POST',
          data: {
            stripeToken: token,
            email: email
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $.ladda('stopAll');
            $(this).parents('body').find('#detailsModal').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong>There has been an error completing your purchase. Please try again later.</div>' );
          },
          success: function(data, textStatus, jqXHR) {
            var id = $form.find('#adid').val()
            var date = new Date();
            date.setTime(date.getTime()+(1*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
            document.cookie = "orderid="+id+expires+"; path=/";
            return data;
          }
        });
      }
    };
  });

  $("#rateYo").rateYo({
    rating: 5,
    halfStar: true,
    onSet: function (rating, rateYoInstance) {
      $('#feedback #rating').val(rating);
    }
  });

  $('#feedback').on('submit', function(){
    var $form = $(this)
    var validator = $form.validate();
    if(validator.form()){
      var l = $(this).find('#submit').ladda();
      l.ladda('start');
      var formdata = $(this).serializeArray();
      $.ajax('https://script.google.com/macros/s/AKfycbyjTWO1bQvspvS-7A9SIjw0iYXblSb4nvyUZYFsCeJqryi9pZLp/exec', {
        type: 'POST',
        data: formdata,
        error: function(jqXHR, textStatus, errorThrown) {
          $.ladda('stopAll');
          $('#page-wrapper').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, there has been an error submitting your feedback. Please try again later.</div>' );
          console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          $.ladda('stopAll');
          swal("Feedback Submitted", "Thanks so much, we appreciate your feedback!", "success")
        }
      });
    }
    return false;
  });

  $(document).on('change', '#classifieds_adtype', function(evt) {
    $.ajax('/update_cats', {
      type: 'GET',
      dataType: 'script',
      data: {
        parenttype: $("#classifieds_adtype option:selected").val()
      },
      error: function(jqXHR, textStatus, errorThrown) {
        return console.log("AJAX Error: " + textStatus);
      },
      success: function(data, textStatus, jqXHR) {
      	data = JSON.parse(data);

        var toAppend = '';
        $.each(data,function(i,o){
           toAppend += '<option value="'+o.id+'">'+o.classname+'</option>';
        });

        document.getElementById('cat_select').disabled = false;

        $('#cat_select').empty().html('').append(toAppend).select2("val", "");

      }
    });
  });
