// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require select2
//= require bootstrap-sprockets
//= require metisMenu/jquery.metisMenu.js
//= require pace/pace.min.js
//= require slimscroll/jquery.slimscroll.min.js
//= require inspinia.js
//= require vide/jquery.vide.min.js
//= require ladda/spin.min.js
//= require ladda/ladda.min.js
//= require ladda/ladda.jquery.min.js
//= require iCheck/icheck.min.js
//= require steps/jquery.steps.min.js
//= require summernote/summernote.min.js
//= require validate/jquery.validate.js
//= require cropper/cropper.min.js
//= require fullcalendar/moment.min.js
//= require slick/slick.min.js
//= require daterangepicker/daterangepicker.js
//= require card/jquery.card.js
//= require canvasblob/js/canvas-to-blob.js
//= require blueimp/jquery.blueimp-gallery.min.js
//= require sweetalert/sweetalert.min.js
//= require flot/jquery.flot.js
//= require flot/jquery.flot.tooltip.min.js
//= require flot/jquery.flot.resize.js
//= require flot/jquery.flot.pie.js
//= require flot/jquery.flot.time.js
//= require flot/jquery.flot.spline.js
//= require sparkline/jquery.sparkline.min.js
//= require ionRangeSlider/ion.rangeSlider.min.js
//= require dotdotdot/jquery.dotdotdot.min.js
//= require leafletMarkercluster/leaflet.markercluster.js
//= require rateyo/jquery.rateyo.min.js
//= require anystretch/anystretch.min.js
//= require bootstrapTour/bootstrap-tour.min.js
//= require blob/blob.js
//= require geocomplete/jquery.geocomplete.min.js
//= require clockpicker/bootstrap-clockpicker.min.js
//= require_tree .

//var ready;
//ready = function() {
    // $("#complete").autocomplete({
    //   source: function(request, resoponse){
    //     $.ajax({
    //       url: '/products/autocomplete.json',
    //       dataType: 'jsonp',
    //       data: { term: request.term },
    //       success: function(data){
    //         var rows = [];
    //         for(var i = 0; i < data.length; i++){
    //           rows.push(data[i].name);
    //         }
    //         resoponse(rows);
    //       }
    //     });
    //   }
    // });
  /*$('#q').typeahead({
       name: 'twitter-oss',
       remote: '/search/autocomplete.json?q=%QUERY',
       valueKey: 'name',
       template: '<p><strong>{{name}}</strong></p>',
       engine: Hogan,
       limit: 10
    });*/

  $( 'button[type=submit]' ).ladda( 'bind' );
  
  var formField = $("#feedback_form");

  $('.dropdown-toggle').dropdown();

  window.setTimeout(function() { $(".alert").alert('close'); }, 3000);

  $('.typeahead').autocomplete({
    minLength: 3,
    source: function(request, response){
      $.ajax({
          type: "GET",
          url: "/search/autocomplete",
          dataType: "json",
          data: {
              term: request.term
          },
          success: function(data) {
              response($.map(data, function(c) {
                if(c.hasOwnProperty('adtitle')){
                  return {label: c.adtitle};
                }
                if(c.hasOwnProperty('name')){
                  if(c.hasOwnProperty('golfclub')){
                    return {label: c.name+' - '+c.golfclub, value:c.name};
                  }else{
                    return {label: c.name};
                  }
                }
              }));
          }
      });
    }
    /*response: function( event, ui ) {
      for(item in ui.content){
        if(c.hasOwnProperty('adtitle')){
          console.log(ui.content[item].adtitle)
          return {label: ui.content[item].adtitle};
        }
        if(ui.content[item].hasOwnProperty('name')){
          console.log(ui.content[item].name)
          return {label: ui.content[item].name};
        }
        if(ui.content[item].hasOwnProperty('golfclub')){
          console.log(ui.content[item].golfclub)
          return {label: ui.content[item].golfclub};
        }
      }*/
    //}
  });

  formField.on('submit', function(e){
    var $form = $(this)
    var validator = $form.validate();
    if(validator.form()){
      var formdata = $(this).serializeArray();
      $(this).find('#loading').hide().removeClass('hide').fadeIn();
      $.ajax('/submithelp', {
        type: 'POST',
        data: formdata,
        error: function(jqXHR, textStatus, errorThrown) {
          $.ladda('stopAll');
          $('#loading').hide().addClass('hide');
          $('#myModal5').prepend( '<div class="alert alert-danger fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Error</strong> Sorry, there has been an error submitting your help ticket. Please try again later.</div>' );
          console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          $.ladda('stopAll');
          $('#page-wrapper').prepend( '<div class="alert alert-success fade in" role="alert"><button type="button" class="close" data-dismiss="alert">×</button><strong>Success</strong> Your ticket has been submitted. We will respond asap.</div>' ); 
          $('#loading').hide().addClass('hide');
          $('#myModal5').modal('hide');
        }
      });
    }
    return false;
  });

//}
//$(document).on('page:load', ready)