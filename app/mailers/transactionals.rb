class Transactionals < ApplicationMailer
  before_filter :set_admin
  include Devise::Mailers::Helpers
  default from: 'accounts@profinder.golf'

  def pro_contact(proemail, proname, user, subject, message)
    @proemail = proemail
    @proname = proname
    @themessage = message
    @subject = subject
    @user = user

    mail(to: @proemail, subject: "Pro Finder Golf - Vistor Contact Message", reply_to: @user)
  end
 
  def new_pro_email(email, password)
  	@password = password
  	@email = email
  	@login = "#{@url}/login"
    mail(to: email, subject: "Welcome to #{@company} - Your Account Information")
  end

  def pro_account_change(profile, plan)
    @email = profile.user.email
    @planTitle = plan.namespace
    @price = plan.price
    mail(to: @email, subject: "#{@company} - Account Subscription Change")
  end

  def customer_item_purchase(user,params, item, seller)
    @email = params[:email]
    @params = user
    @product = item
    @seller = seller.profile
    @selleruser = seller
    @sellerEmail = seller.email
    latLon = "#{@seller.latitude}, #{@seller.longitude}"
    @address = "#{Geocoder.search(latLon).first.city}<br/>#{Geocoder.search(latLon).first.sub_state}"
    mail(to: @email, subject: "#{@company} - Your Purchase Reciept")    
  end

  def seller_item_purchase(user,params, item, seller)
    @email = params[:email]
    @params = user
    @product = item
    @seller = seller.profile
    @selleruser = seller
    @sellerEmail = seller.email
    latLon = "#{@seller.latitude}, #{@seller.longitude}"
    @address = "#{Geocoder.search(latLon).first.city}<br/>#{Geocoder.search(latLon).first.sub_state}"
    mail(to: @email, subject: "#{@company} - Item Sold Reciept")   
  end

  def addon_purchase(addons,params,profile,price,days,credits)
    @email = profile.user.email
    @profile = profile
    @addons = addons
    @price = price
    @days = days
    latLon = "#{@profile.latitude}, #{@profile.longitude}" 
    @address = Geocoder.search(latLon)
    @credits = credits
    mail(to: @email, subject: "#{@company} - Advert Addons Reciept")   
  end

  def confirmation_instructions(record, token, opts={})
    @token = token
    devise_mail(record, :confirmation_instructions, opts)
  end

  def reset_password_instructions(record, token, opts={})
    @token = token
    @email = record.email
    @resource = record

    mail(to: record.email, subject: "Welcome to #{@company} - Your Account Information")
  end

  def unlock_instructions(record, token, opts={})
    @token = token
    devise_mail(record, :unlock_instructions, opts)
  end

  def password_change(record, opts={})
    devise_mail(record, :password_change, opts)
  end

  def set_admin
  	@company = "Pro Finder Golf"
  	@company_domain = "Profinder.golf"
	  @url = 'https://www.profinder.golf'
    @logoimg = "https://s3-eu-west-1.amazonaws.com/pro-finder-golf/email/logo.png"
    #@url = 'http://local.profinder.golf'
  end

end
