class RegistrationsController < Devise::RegistrationsController
  
  def edit
  	Stripe.api_key = Rails.configuration.stripe[:secret_key]
    if !defined?(@profile.stripe_id)
    	customer = Stripe::Customer.retrieve(@profile.stripe_id)
    	@stripe_id = @profile.stripe_id
    	@card = customer.sources.retrieve(customer["default_source"])
    end
  end

  protected

  def after_sign_up_path_for(resource)
  	edit_profile_path(resource)
  end
end