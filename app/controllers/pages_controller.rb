class PagesController < ApplicationController
  require 'json'

  def home
  
  	highlights = Classifieds.where(:status => true).joins(:addons).merge(Addons.where(:highlight => true))

  	count = highlights.count
  	count -= 1

	  @offset = (0..count).to_a.shuffle.take(3)

    @latestPros = Profile.last(3).reverse

	  @highAds = highlights.values_at(*@offset)
  end

  def getBlogPost
    req = Net::HTTP.get(URI.parse("https://blog.profinder.golf/wp-json/wp/v2/posts?limit=3"))
    @rss = JSON.parse(req)
    render 'pages/latestPosts'
  end

  def signup
    	
  end

end
