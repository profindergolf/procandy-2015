class ClassifiedsController < ApplicationController
	require 'sanitize'
	require 'json'

	before_action :authenticate_user!, except: [:show, :update_cats, :confirmPurchase, :stripeProcess, :thankyou]
	before_action :check_account_level, only: [:new, :create]

	respond_to :html, :json

	def index
		@classAds = Classifieds.where(user_id: current_user.id)
		@limit = @profile.plans.ads <= @classAds.size ? true : false
		if !@classAds.nil?
			@classAds = @classAds.order('created_at').page(params[:page]).per(10)
		end
	end

	def show
		@classAd = Classifieds.find_by_id_and_status(params[:id], 1) or not_found
		@images = Images.where(id: @classAd.image).take
		@extraImgs = Adextraimgs.where(classifieds_id: @classAd.id).order("created_at ASC")

		@classTypes = Classtypes.all
		@tags = Tag.all
		if !@classAd.nil? and !@classAd.adtype.nil?
			@classCats = Classcat.where("parenttype= ?", @classAd.adtype)
		end
		@brands = Brand.all
		if @images.nil? 
			@image = Images.new
		end

		event = Analytic.new
		event.analytic_type = 2
		if !cookies[:classAnalytics]
			event.identifier = SecureRandom.hex
			cookies[:classAnalytics] = event.identifier
		else
			event.identifier = cookies[:classAnalytics]
		end
		event.profiles_id = @classAd.user.profile.id
		event.classifieds_id = @classAd.id
		event.properties = {test: '123'}
		event.save

	end

	def new
		@classformpath = classifieds_path
		@classAd = Classifieds.new
		@classTypes = Classtypes.all
		@tags = Tag.all
		@addons = Addons.all
		@brands = Brand.all
		@image = Images.new
		@limitNum = @profile.plans.photos
		@limit = false
		@classAd.adtype = 0
	end

	def edit
		@classformpath = classified_path
		@classAd = Classifieds.find_by_id_and_user_id(params[:id], current_user.id)
		@images = Images.find_by_id(@classAd.image)
		@extraImgs = Adextraimgs.where(classifieds_id: @classAd.id).order("created_at DESC")
		@limit = @profile.plans.photos <= @extraImgs.size ? true : false
		@limitNum = @profile.plans.photos
		@classTypes = Classtypes.all
		@addons = Addons.all
		@tags = Tag.all
		if !@classAd.adtype.blank?
			@classCats = Classcat.where("parenttype= ?", @classAd.adtype)
		end
		@brands = Brand.all
		if @image.nil?
			@image = Images.new
		end
	end

	def create

		@imageDetails = ''

		if !params[:classifieds][:theimage].blank?
			image = Images.new()
			image.user_id = current_user.id
			image.theimage = params[:classifieds][:theimage]
			image.protype = 'classimg'
			if image.save
			   	checking = true
			else
			    checking = false
			end
		end

		if !params[:classifieds][:remote_url].blank?
			image = Images.new()
			image.user_id = current_user.id
			image.protype = 'classimg'
			image.remote_theimage_url = params[:classifieds][:remote_url]
			image.save
		end	

		classAd = Classifieds.new(classad_params)
		classAd.user_id = current_user.id
		classAd.status = 1
		if !image.blank?
			classAd.image = image.id
		end

		classAd.url = classAd.url.gsub(/\?.*/, '')
		if classAd.save

			if !params[:classifieds][:extraImage].blank?
				params[:classifieds][:extraImage].each do |exImg|
					images = Adextraimgs.where(classifieds_id: classAd.id)
					limit = @profile.plans.photos <= images.size ? true : false

					if !limit
						image = Images.new(theimage: exImg[:data])
						image.user_id = current_user.id
						image.protype = 'adextraimg'
						if image.save
							adextraimg = Adextraimgs.new()
							adextraimg.classifieds_id = classAd.id
							adextraimg.images_id = image.id
							adextraimg.save
							checking = true
						else
							checking = false
						end 
					end
				end
			end
			
			if !params[:classifieds][:brand].blank?
				brandrel = Brandrel.new()
				brandrel.classifieds_id = classAd.id
				brandrel.brand_id = params[:classifieds][:brand]
				if brandrel.save
					checking = true
				else
					checking = false
				end
			end

			if !params[:classifieds][:tags].blank?
				params[:classifieds][:tags].each do |tag|
					classtags = Classadtag.new()
					classtags.classifieds_id = classAd.id
					classtags.tag_id = tag
					if classtags.save
						checking = true
					else
						checking = false
					end
				end
			end

			if !params[:addon].blank?
				ads = []
				params[:addon].each do |key, value|
					ads.push((value.to_f)/100)
				end
				params[:id] = classAd.id
				@addons = Addons.where(price: ads)

				date1 = (params[:classifieds][:enddate].to_datetime.strftime('%Q').to_f)/1000
				date2 = (Time.now.utc.to_s(:db).to_datetime.strftime('%Q').to_f)/1000

				@dayDist = ((((date1 - date2)/60)/60)/24).ceil
				params[:credits] = @dayDist*params[:addon].count
				#redirect_to "/classifieds/#{@classAd.id}/addons", addon: params[:addon], notice: 'Your Advert has been saved'
				render 'classifieds/addonProcess',  notice: 'Your Advert has been saved. Please confirm your addon purchases'
			else
				redirect_to classifieds_path, notice: 'Your Advert was updated successfully.'
			end
		else
			render :action => 'new', :notice => 'there was a problem'
		end
	end

	def update
		checking = true

		@classAd = Classifieds.find_by_id_and_user_id(params[:id], current_user.id)

		if !params[:classifieds][:theimage].blank?

			theimage = Images.exists?( :id => @classAd.image )
			if theimage
				destroyImg = Images.find(@classAd.image).destroy
			end
			image = Images.new()
			image.user_id = current_user.id
			image.theimage = params[:classifieds][:theimage]
			image.protype = 'classimg'
			if image.save
			   	checking = true
				@classAd.image = image.id
			else
			    checking = false
			end
		end

		if !params[:classifieds][:remote_url].blank?
			images = Images.find_by_id(@classAd.image)
			if !images.blank?
				images.remove_theimage!
				images.destroy
			end
			image = Images.new()
			image.user_id = current_user.id
			image.protype = 'classimg'
			image.remote_theimage_url = params[:classifieds][:remote_url]
			image.save
			@classAd.image = image.id
		end

		if !params[:classifieds][:extraImage].blank?
			params[:classifieds][:extraImage].each do |img|
				images = Adextraimgs.where(classifieds_id: @classAd.id)
				@limit = @profile.plans.photos <= images.size ? true : false

				if !@limit
					image = Images.new(theimage: img[:data])
					image.user_id = current_user.id
					image.protype = 'adextraimg'
					if image.save
						adextraimg = Adextraimgs.new()
						adextraimg.classifieds_id = @classAd.id
						adextraimg.images_id = image.id
						adextraimg.save
						checking = true
					else
						checking = false
					end 
				end
			end
		end

		if !@classAd.tag.blank?
			@classAd.tag.clear
		end

		if !params[:classifieds][:tags].blank?
			params[:classifieds][:tags].each do |tag|
				classtags = Classadtag.new()
				classtags.classifieds_id = params[:id]
				classtags.tag_id = tag
				if classtags.save
					checking = true
				else
					checking = false
				end
			end
		end

		if !@classAd.brand.blank?
			@classAd.brand.clear
		end

		if !params[:classifieds][:brand].blank?
			brandrel = Brandrel.new()
			brandrel.classifieds_id = params[:id]
			brandrel.brand_id = params[:classifieds][:brand]
			if brandrel.save
				checking = true
			else
				checking = false
			end
		end

		params[:classifieds][:description] = Sanitize.fragment(params[:classifieds][:description], Sanitize::Config::RESTRICTED)

		if @classAd.enddate.to_date.future?
			@classAd.status = 1
		else
			Classifieds.searchkick_index.remove(@classAd)
		end

		if @classAd.update(classad_params)
			checking = true
		else
			checking = false
		end

		redirect_to classifieds_path, notice: 'Your Advert was updated successfully.'
		return false
	end

	def confirmPurchase
		@product = Classifieds.find_by_id_and_status(params[:prod][:id], 1) or not_found
		@images = Images.where(id: @product.image).take
	end

	def processAddon
		if params[:id].kind_of?(Array) 
			ids = eval(params[:aid])
		else
			ids = params[:aid] 
		end
		
		addons = Addons.where(id: ids)

		if !addons.nil?

			price = addons.sum(:price)

			date1 = (params[:enddate].to_datetime.strftime('%Q').to_f)/1000
			date2 = (Time.now.utc.to_s(:db).to_datetime.strftime('%Q').to_f)/1000

			@dayDist = ((((date1 - date2)/60)/60)/24).ceil

			begin

				Stripe.api_key = Rails.configuration.stripe[:secret_key]
				Stripe::Charge.create({
				  :amount => (price*100).to_i,
				  :currency => "gbp",
				  :source => params[:stripeToken]
				})

	        rescue Stripe::CardError => e
				flash[:error] = e.message
				render js: "window.location = '#{classifieds_path}'"
				#redirect_to classifieds_path, notice: 'There was an error in completing the addon purchase'
				return
			else

				Transactionals.addon_purchase(addons,params,@profile,price,@dayDist,false).deliver_now

				addons.each do |addon|
					adAddons = Advertaddon.new()
					adAddons.classifieds_id = params[:id]
					adAddons.addons_id = addon.id
					adAddons.save
				end
				flash[:success] = "Thank You, you addons have now been activated on your advert."
				render js: "window.location = '#{classifieds_path}'"
				#redirect_to classifieds_path, :notice => 'Thank You, you addons have now been activated on your advert.'
			end
		else
			flash[:error] = 'There was an error in completing the addon purchase'
			render js: "window.location = '#{classifieds_path}'"
			#redirect_to classifieds_path, notice: 'There was an error in completing the addon purchase'
		end
	end

	def processAddonCredits
		ids = eval(params[:aid])

		addons = Addons.where(id: ids)


		date1 = (params[:enddate].to_datetime.strftime('%Q').to_f)/1000
		date2 = (Time.now.utc.to_s(:db).to_datetime.strftime('%Q').to_f)/1000

		@dayDist = ((((date1 - date2)/60)/60)/24).ceil
		credits =  @dayDist*addons.count

		if !addons.nil?

			if @profile.adcredits - credits > 0 
				
				@profile.adcredits = @profile.adcredits - credits
				@profile.save

				addons.each do |addon|
					adAddons = Advertaddon.new()
					adAddons.classifieds_id = params[:adid]
					adAddons.addons_id = addon.id
					adAddons.save
				end	

				Transactionals.addon_purchase(addons,params,@profile,price,@dayDist,credits).deliver_now

				flash[:success] = "Thank You, you addons have now been activated on your advert."
				render js: "window.location = '#{classifieds_path}'"		
			else
				render :json => { :success => false, notice: "Sorry, you appear to not have enough credits on your account to complete this purchase"}
			end
			
		else
			render :json => { :success => false, notice: "There was an error in completing the addon purchase"}
		end
	end

	def destroy
		Classifieds.find_by_id_and_user_id(params[:id], current_user.id).destroy
		redirect_to classifieds_path, notice: 'Your Advert was deleted successfully.'
	end

	def changeArchive
		classAd = Classifieds.find_by_id_and_user_id(params[:adid], current_user.id)
		if params[:status].to_f != 1
			classAd.status = 0
		else
			classAd.status = 1
		end
		if classAd.save
			render json: {status: true}
		else
		    render json: {error: classAd.errors, status:false}
		end
	end

	def update_cats
		@classCats = Classcat.where("parenttype= ?", params[:parenttype])
	    respond_to do |format|
	    	format.json { render json: @classCats}
	    end
	end

	def getebayitem
		require 'net/http'
		url = "http://open.api.ebay.com/shopping?callname=GetSingleItem&responseencoding=JSON&appid=profinde-0364-4943-80d1-2f00aabac5ee&siteid=3&version=934&ItemID=#{params[:ebayid]}&IncludeSelector=TextDescription,ItemSpecifics"
		req = Net::HTTP.get(URI.parse(url))
		render json: req
	end

	def getgbitem
		require 'net/http'
		# https://api.import.io/store/connector/ecfcfc48-8ce6-4ce0-b81f-261f155bf225/_query?format=JSON&_apikey=beb078f4fbe14423a39536d7af7caa85d013f3094eda5711be11a0014743bab27760cc16d8dab045dae575666ba457eb9737b9d31485219ada5b47fbe7c0fc67e773dd390d0130ab0e3cde674e57bac8&input=webpage/url:http://www.golfbidder.co.uk/product/636309/Titleist_Vokey_Spin_Milled_Tour_Chrome_(2009).html
		url = "https://api.import.io/store/connector/ecfcfc48-8ce6-4ce0-b81f-261f155bf225/_query?format=JSON&_apikey=#{ENV['IMPORTIOKEY']}&input=webpage/url:#{params[:ebayid]}"
		#url = "https://api.import.io/store/data/ecfcfc48-8ce6-4ce0-b81f-261f155bf225/_query?input=%7Bwebpage%2Furl%7D%3A%7B%22#{params[:ebayid]}%22%7D&_apikey="
		req = Net::HTTP.get(URI.parse(url))
		respond_to do |format|
			hash = JSON.parse req
			require 'open-uri'
			file = File.extname(hash['results'][0]['productpic'])
			filename = "#{SecureRandom.hex}#{file}"
			open("#{Rails.root}/public/tmp/#{filename}", 'wb') do |file|
			  file << open(hash['results'][0]['productpic']).read
			end
			hash['results'][0]['productpic'] = "/tmp/#{filename}"
	    	format.json { render json: hash}
	    end
	end

	def stripeProcess
		classAd = Classifieds.find_by_id_and_status(params[:id], 1)

		if !classAd.nil?

			begin
				Stripe.api_key = Rails.configuration.stripe[:secret_key]
				charge = Stripe::Charge.create({
				  :amount => (classAd.price*100).ceil,
				  :currency => "gbp",
				  :source => params[:stripeToken],
				  :receipt_email => params[:email],
				  :destination => classAd.user.uid,
				  :application_fee => (((classAd.price/100)*2)*100).ceil,
				})

				sold = Solditem.new()
				sold.id = classAd.id
				sold.user_id = classAd.user_id
				sold.adtitle = classAd.adtitle
				sold.price = classAd.price
				sold.description = classAd.description
				sold.image = classAd.image
				sold.url = classAd.url
				sold.adtype = classAd.adtype
				sold.cat = classAd.cat
				sold.stripe_id = charge.id
				
				stripeCharge = Stripe::Charge.retrieve(charge.id)
				@charge = JSON.parse stripeCharge.to_s

				if sold.save
					data = params
					Transactionals.customer_item_purchase(@charge,params,sold, classAd.user).deliver_now
					Transactionals.seller_item_purchase(@charge,params,sold, classAd.user).deliver_now
				end

				classAd.status = 2;
				classAd.save

				Classifieds.searchkick_index.remove(classAd)

				event = Analytic.new
				event.analytic_type = 3
				if !cookies[:classAnalytics]
					event.identifier = SecureRandom.hex
					cookies[:classAnalytics] = event.identifier
				else
					event.identifier = cookies[:classAnalytics]
				end

				event.profiles_id = classAd.user.profile.id
				event.classifieds_id = classAd.id
				event.properties = {test: '123'}
				event.save

				flash[:success] = 'Thank You for your purchase. You will recieve an email confirming your purchase soon.'
				render js: "window.location = '/purchase/#{sold.id}/thankyou'"
	        rescue Stripe::CardError => e
				flash[:error] = e.message
				render :json => { :success => false, notice: "There was an error in completing the addon purchase"}
			end
		else
			render :json => { :success => false, notice: "There was an error in completing the addon purchase"}
		end
	end

	def extraImageDelete
		adextraimg = Adextraimgs.find_by_classifieds_id_and_images_id(params[:id].to_i, params[:imgid].to_i)

		puts adextraimg.images_id.inspect

		image = Images.find(adextraimg.images_id)

		if image.destroy
			adextraimg.destroy
			render json: {status: true}
		else
			render json: {status: false, error: true}
		end

	end

	def soldInvoices
		@sold = Solditem.all.page(params[:page]).per(10)
	end

	def viewInvoice
		@invoice = Solditem.find_by_id_and_user_id(params[:id], current_user.id)
		Stripe.api_key = Rails.configuration.stripe[:secret_key]
		stripeCharge = Stripe::Charge.retrieve(@invoice.stripe_id)
		@charge = JSON.parse stripeCharge.to_s
	end

	def processRefund
		invoice = Solditem.find_by_id_and_user_id(params[:id], current_user.id)
		Stripe.api_key = Rails.configuration.stripe[:secret_key]
		refund = Stripe::Refund.create(
		  charge: invoice.stripe_id
		)

		invoice.refund = true
		if invoice.save
			render json: {status: true}		
		else
			render json: {status: false, error: true}
		end
	end

	def thankyou
		if cookies[:orderid] != params[:id]
			not_found
		end
		@sold = Solditem.find_by_id(params[:id]) or not_found
		Stripe.api_key = Rails.configuration.stripe[:secret_key]
		stripeCharge = Stripe::Charge.retrieve(@sold.stripe_id)
		@charge = JSON.parse stripeCharge.to_s
		@seller = User.find_by_id(@sold.user_id)
		puts @charge.inspect
	end

  	protected

 		def sanitize_page_params
	    	params[:id] = params[:id].to_i
	  	end

	  	def image_params
			params.require(:classifieds).permit(:theimage, :protype, :user_id, :remote_theimage_url, :adtitle, :price, :description, :cat, :image, :vendor, :url, :status, :adtype, :remote_url, :external_id)
		end

		def classad_params
	  		params.require(:classifieds).permit(:adtitle, :price, :description, :cat, :image, :vendor, :url, :status, :adtype, :remote_url, :external_id, :enddate)
		end

	  	def authenticate_user!
		    if user_signed_in?
		      super
		    else
		      redirect_to new_user_session_path, :notice => 'You don\'t have access to furfill that rqequest'
		      ## if you want render 404 page
		      ## render :file => File.join(Rails.root, 'public/404'), :formats => [:html], :status => 404, :layout => false
		    end
	  	end	

	  	def check_account_level
	  		planLimits = @profile.plans
	  		count = Classifieds.where(user_id: current_user.id).size

	  		if count >= planLimits.ads
	  			redirect_to classifieds_path, :notice => 'You have reached the maxium amount of adverts you can create. Please upgrade your account to create more.'
	  			return false
	  		end
	  	end
end
