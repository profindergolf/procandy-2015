class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :get_profile

  def after_sign_in_path_for(resource)    
  	if resource.is_a?(AdminUser)
  		admin_dashboard_path
  	else
      @profile = Profile.find_by user_id: current_user.id
      if !@profile.wizard.blank?
        cookies[:dashboardtour] = true
  		  edit_profile_path(@profile.id)
      else
        dashboard_path()
      end
  	end
  end

  def create_groove_ticket 
    client = Freshdesk.new("https://profindergolf.freshdesk.com/", ENV['freshdesk_key'])

    client.post_tickets(:email => params['email'], :subject => params['subject'], :description => params['message'], :name => params['email'], :status => 2, :priority => 1)

    puts client.inspect
    render json: client
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  private

    def get_profile
      if user_signed_in?
        @profile = Profile.find_by user_id: current_user.id
      end
    end
end
