class SubscriptionController < ApplicationController

  def upgrade
    @user = current_user

    @upgradePlans = Plans.all().order("id asc")
    @goldPlan = Plans.find_by id: 2
  end

  def cardChange
    Stripe.api_key = Rails.configuration.stripe[:secret_key]
    customer = Stripe::Customer.retrieve(params[:stripe_id])
    customer.sources.retrieve(customer["default_source"]).delete()
    customer.sources.create(:source => params[:token])
    flash[:success] = "Your subscription payment method has been updated successfully."
    render json: {status: true}
  end

  def subscribe
  	@user = current_user
    token = params[:stripeToken]
    level = params[:planId]
    planId = params[:planId]

    planCheck = Plans.find_by id: planId

    if planId.to_f < 2
  
      if @profile.stripe_id?
        customer = Stripe::Customer.retrieve(@profile.stripe_id)
        customer.subscriptions.retrieve(customer.subscriptions.data[0].id).delete
      end

      Classifieds.where(id: params[:id], user_id: current_user.id, status: 0).destroy_all
      adsCount = Classifieds.where(id: params[:id], user_id: current_user.id).size

      if planCheck.ads > adsCount
        newest_n_records = Classifieds.where(id: params[:id], user_id: current_user.id).order("created_at DESC").limit(planCheck.ads)
        Classifieds.destroy_all(['id NOT IN (?)', newest_n_records.collect(&:id)])
      end

      servSize = @profile.services.size

      if servSize > planCheck.service
        newest_n_records = Proserv.where(profile_id: current_user.id).order("created_at DESC").limit(planCheck.service)
        Proserv.destroy_all(['id NOT IN (?)', newest_n_records.collect(&:id)])
      end

      @profile.featured = false

    else

      if !@profile.stripe_id?

        begin
          customer = Stripe::Customer.create(
            :source => token,
            :plan => "pfg-#{params[:planName]}",
            :email => @user.email
          )
          @profile.stripe_id = customer.id

        rescue Stripe::CardError => e
          flash[:error] = e.message
          redirect_to users_upgrade_path, notice: 'There was an error in upgrading your account.'
          return
        end

      else
        customer = Stripe::Customer.retrieve(@profile.stripe_id)
        puts customer.inspect
        if defined? customer.subscriptions.data[0].id
          subscription = customer.subscriptions.retrieve(customer.subscriptions.data[0].id)
          subscription.plan = "pfg-#{params[:planName]}"
          subscription.save
        else
          customer.subscriptions.create(:plan => "pfg-#{params[:planName]}")
        end
      end

      
      if planId.to_f >= 2
        @profile.featured = true
      else
        @profile.featured = false
      end

    end

    @profile.level = planId

    if @profile.save
      Transactionals.pro_account_change(@profile,planCheck).deliver_now
      redirect_to users_upgrade_path, notice: 'Account upgraded successfully!'
    else
      redirect_to users_upgrade_path, notice: 'There was an error in upgrading your account.'
    end
  end

  private

  	def profile_params
    		params.require(:profile).permit(:latitude, :longitude, :postcode, :name, :bio, :golfclub, :phone, :position)
  	end

end