class OmniauthCallbacksController < ApplicationController

  def stripe_connect
    @user = current_user
    if @user.update_attributes({
      provider: request.env["omniauth.auth"].provider,
      uid: request.env["omniauth.auth"].uid,
      access_code: request.env["omniauth.auth"].credentials.token,
      publishable_key: request.env["omniauth.auth"].info.stripe_publishable_key
    })
      # anything else you need to do in response..
      @user.profile.update_attributes({ merchant_setup: true })
      if is_navigational_format?
      	flash[:success] = 'Stripe Merchant Account Completed'
      else
		flash[:warning] = 'There was an error completing your Merchant Account. Please try again later.'
      end
      sign_in_and_redirect dashboard_path(), :event => :authentication
    else
      session["devise.stripe_connect_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def failure
	  flash[:warning] = 'There was an error completing your Merchant Account. Please try again later.'
      sign_in_and_redirect dashboard_path(), :event => :authentication
  end

end
