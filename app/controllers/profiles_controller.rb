class ProfilesController < InheritedResources::Base
	before_filter :sanitize_page_params

	before_action :authenticate_user!, except: [:show, :emailContact]
	before_action :set_user, only: [:edit, :update, :profileoverview, :galleryOverview, :galleryImageDelete, :galleryImageUpload, :deleteStatus, :createNewStatus]
	before_action :authorize_user, except: [:index, :show, :profileoverview, :galleryOverview, :galleryImageDelete, :galleryImageUpload, :deleteStatus, :createNewStatus, :emailContact]

	def index

	end

	def new
	    @profiles = current_user.profile.build
	end

	def create
	    @profiles = current_user.profile.build
	end

	def show
		@profile = Profile.find_by id: params[:id]
		@avatar = Avatars.where(user_id: @profile.user.id, protype: 'avatar').take
		@images = Progallery.where(user_id: @profile.user.id, protype: 'progallery').order("created_at DESC")
		@ads = Classifieds.where(user_id: @profile.user.id, status: 1).order("created_at DESC").page(params[:page]).per(10)


		event = Analytic.new
		event.analytic_type = 1
		if !cookies[:profileAnalytics]
			event.identifier = SecureRandom.hex
			cookies[:profileAnalytics] = event.identifier
		else
			event.identifier = cookies[:profileAnalytics]
		end
		event.profiles_id = params[:id]
		event.classifieds_id = 0
		if session[:searchQuery]
			event.properties = Hash.new
			event.properties[:searchQuery] = session[:searchQuery]
		end
		# = {test: '123'}
		event.save
	end

	def edit
	   	@profiles = @profile
		@avatar = Avatars.where(user_id: current_user.id, protype: 'avatar').take
		@brands = Brand.all
		@services = Services.all
	   	@attributes = Profile.attribute_names - %w(id user_id created_at updated_at)
	end

	def update

		params[:profile][:bio]= ActionController::Base.helpers.sanitize(params[:profile][:bio], tags: %w(strong em ul li p br i u))

		profile = @profile.update(profile_params)
		profile = current_user.profile
		profile.times = params[:profile][:times]

		if profile.wizard == 1
			redirectdash = true
		else
			redirectdash = false
		end

		profile.wizard = 0
		profile.save
		profile = current_user.profile

		if !params[:profiles][:theimage].blank?
			@images = Avatars.where(user_id: current_user.id, protype: 'avatar').delete_all
			image = Avatars.new()
			image.user_id = current_user.id
			image.theimage = params[:profiles][:theimage]
			image.protype = 'avatar'
			if image.save
			   	checking = true
			else
			    checking = false
			end
		end	
		if !params[:profiles][:brands].blank?
			probrands = Profilebrand.where('profile_id': profile.id).delete_all

			params[:profiles][:brands].each do |brand|
				probrand = Profilebrand.new()
				probrand.profile_id = profile.id
				probrand.brand_id = brand
				if probrand.save
					@checking = true
				else
					@checking = false
				end
			end			
		end
		if !params[:service].blank?
			proservs = Proserv.where('profile_id': profile.id).delete_all

			params[:service].each do |serv|
				newproserv = Proserv.new()
				newproserv.profile_id = profile.id
				newproserv.services_id = serv[0]
				if newproserv.save
					@checking = true
				else
					@checking = false
				end
			end
		end

		if defined?(redirectdash)
			redirect_to dashboard_path(tour: "true"), tour: "true", notice: 'Your profile was successfully updated.'
		else
			redirect_to my_profile_path, notice: 'Your profile was successfully updated.'
		end

		#respond_to do |format|
	      #if @profiles.update(profile_params)
	        #format.html { redirect_to @profiles, notice: 'Your profile was successfully updated 2.' }
	        #format.json { render :show, status: :updated, location: @profiles }
	      #else
	        #format.html { render :new }
	        #format.json { render json: @profiles.errors, status: :unprocessable_entity }
	      #end
	    #end
	end

	def profileoverview
		@avatar = Avatars.where(user_id: current_user.id, protype: 'avatar').take
	   	@attributes = Profile.attribute_names - %w(id user_id created_at updated_at)
	end

	def galleryOverview
		@images = Progallery.where(user_id: @profiles.id, protype: 'progallery').order("created_at DESC")
		@limit = @profile.plans.photos <= @images.size ? true : false
	end

	def galleryImageUpload
		images = Progallery.where(user_id: params[:id], protype: 'progallery')
		@limit = @profile.plans.photos <= images.size ? true : false

		if !@limit
			if params[:croppedImage].try(:original_filename) == 'blob'
			  params[:croppedImage].original_filename << '.png'
			end
			image = Progallery.new(theimage: params[:croppedImage])
			image.user_id = params[:id]
			image.protype = 'progallery'
			if image.save
				render json: {status: true}
			else
				render json: {status: false, error: true}
			end 
		else
			render json: {status: false, error: true}
		end
	end

	def galleryImageDelete
		if Progallery.find(params[:imgid]).destroy
			render json: {status: true}
		else
			render json: {status: false, error: true}
		end

	end

	def createNewStatus
		puts params.inspect
		check = Profilestatus.where(profile_id: params[:id])
		puts check.inspect
		if !check.blank?
			check.destroy_all
		end
		status = Profilestatus.new(profile_id: params[:id], status: params[:status])
		if status.save
			render json: {status: true, data: status.to_json}
		else
			render json: {status: false, error: true}
		end
	end

	def deleteStatus
		if Profilestatus.find(params[:statusid]).destroy
			render json: {status: true}
		else
			render json: {status: false, error: true}
		end
	end

	def emailContact
		pro = Profile.find_by user_id: params[:id].to_i
		mailSent = Transactionals.pro_contact(pro.user.email, pro.name, params[:user], params[:subject], params[:themessage]).deliver_now
		if mailSent
			render json: {status: true}
		else
			render json: {status: false, error: true}
		end
	end

	private

		def sanitize_page_params
	    	params[:id] = params[:id].to_i
	  	end

	    def set_user
	      	@profiles = @profile
	    end

	    def authorize_user
	      	redirect_to my_profile_path, notice: "This is not the profile you're looking for..." if current_user.profile.id != params[:id]
	    end

		def profile_params
	  		params.require(:profile).permit(:latitude, :longitude, :postcode, :name, :bio, :golfclub, :phone, :position, :fb, :twitter, :youtube, :times)
		end

		def status_params
			params.require(:profilestatus).permit(:user_id, :description)
		end
end

