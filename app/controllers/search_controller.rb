require 'elasticsearch/model'

class SearchController < ApplicationController

	#def autocomplete
		#render json: Profile.search(params[:query], limit: 10)
		#render json: Classifieds.search(params[:query], limit: 10)
	#end

	def autocomplete
		render json: Searchkick.search(params[:term],{
			index_name: [Profile.searchkick_index.name, Classifieds.searchkick_index.name],
			match: :word_start,
			fields: ["adtitle", "name", "golfclub"],
			limit: 10,
			load: false,
			misspellings: {below: 3}
    	})
	end

	def search

		@params = params

		if @params[:page].blank?
			@params[:page] = 0
		end

		if @params[:perpage].blank?
			@params[:perpage] = 20
		end

		price_ranges = [{to: 50}, {from: 50, to: 100},{from: 100, to: 200},{from: 200, to: 300},{from: 300, to: 400}, {from: 400}]

		dist = Hash.new

		[:location, :distance].each do |d|
			dist[d] = params[d] if params[d].present?
		end

		if dist[:distance].nil?
			dist[:distance] = 1
		end

		dist[:location] = Geocoder.search(params[:location]).first if !params[:location].nil? and !Geocoder.search(params[:location]).first.nil?

		boostDist = nil

		distance = Hash.new

		if !dist[:location].nil?
			distance[:location] = Hash.new
			distance[:location][:near] = Hash.new
			distance[:location][:near][:lat] = dist[:location].coordinates[0]
			distance[:location][:near][:lon] = dist[:location].coordinates[1]
			boostDist = Hash.new
			boostDist[:lat] = dist[:location].coordinates[0]
			boostDist[:lon] = dist[:location].coordinates[1]
			miles = dist[:distance].to_f * 1.60934
			distance[:location][:within] = Hash.new
			distance[:location][:within] = "#{miles}km"
		end

	  	if params[:q].to_s.strip.length == 0
	  		keyword = '*'
	  	else
	  		session[:searchQuery] = params[:q]
	  		keyword = params[:q]
	  	end
		
		if params[:type] == 'prods' 

		  	options = Hash.new
		  	[:tags].each do |s|
		  		options[s] = params[s] if params[s].present?
			end

			if !distance[:location].nil?
				options[:location] = distance[:location]
			end

			if !params[:brandname].blank?
				options[:brandname] = []
				params[:brandname].each do |brand|
					options[:brandname].push(brand) 
				end
			end

			if !boostDist.nil?
		  		@results = Classifieds.search keyword, where:options, boost_by_distance: {field: :location, origin: boostDist}, misspellings: {edit_distance: 2}, aggs: {price: {ranges: price_ranges}, tags:{}, category:{}, category_parent:{},brandname:{}}, highlight: true, suggest: true, boost_where: {featured: {value: 1, factor: 100}, profeatured: {value: 1, factor: 50}}, smart_aggs: false, match: :word_start, page: params[:page], per_page: @params[:perpage]
		  	else
		  		@results = Classifieds.search keyword, where:options, misspellings: {edit_distance: 2}, aggs: {price: {ranges: price_ranges}, tags:{}, brandname:{}, category:{}, category_parent:{}}, highlight: true, suggest: true, boost_where: {featured: {value: 1, factor: 100}, profeatured: {value: 1, factor: 50}}, smart_aggs: false, match: :word_start, page: params[:page], per_page: @params[:perpage]
		  	end

		  	@aggs = @results.aggs
		  	@aggKeys = @aggs.keys

		    if @results.suggestions != []
		    	@suggestions = @results.suggestions[0]
		   	end

	   	elsif params[:type] == 'pros' 

		  	options = Hash.new
		  	[:services].each do |s|
		  		options[s] = params[s] if params[s].present?
			end

			if !params[:brandname].blank?
				options[:brandname] = []
				params[:brandname].each do |brand|
					options[:brandname].push(brand) 
				end
			end

			if !distance[:location].nil?
				options[:location] = distance[:location]
			end

			if !boostDist.nil?
		    	@results = Profile.search keyword, where:options, boost_by_distance: {field: :location, origin: boostDist}, misspellings: {edit_distance: 2}, aggs: {services:{},brandname:{}}, highlight: true, suggest: true, boost_where: {featured: {value: 1, factor: 100}}, smart_aggs: false, match: :word_start, page: params[:page], per_page: @params[:perpage]
		    else
		    	@results = Profile.search keyword, where:options, misspellings: {edit_distance: 2}, aggs: {services:{},brandname:{}}, highlight: true, suggest: true, boost_where: {featured: {value: 1, factor: 100}}, smart_aggs: false, match: :word_start, page: params[:page], per_page: @params[:perpage]
		    end

		  	@aggs = @results.aggs
		  	@aggKeys = @aggs.keys

		    if @results.suggestions != []
		    	@suggestions = @results.suggestions[0]
		    end

		else

			@params[:type] = 'all'

		  	options = Hash.new
		  	[:tags, :services].each do |s|
		  		options[s] = params[s] if params[s].present?
			end

			if !params[:brandname].blank?
				options[:brandname] = []
				params[:brandname].each do |brand|
					options[:brandname].push(brand) 
				end
			end

			if !distance[:location].nil?
				options[:location] = distance[:location]
			end

			if !distance[:location].nil?
				@results = Searchkick.search keyword, where:options, index_name: [Classifieds.searchkick_index.name, Profile.searchkick_index.name], boost_by_distance: {field: :location, origin: boostDist}, misspellings: {edit_distance: 2}, aggs: {price: {ranges: price_ranges}, tags:{}, brandname:{}, category:{}, category_parent:{},services:{}}, highlight: true, suggest: true, boost_where: {featured: {value: 1, factor: 100}, profeatured: {value: 1, factor: 50}}, smart_aggs: false, match: :word_start, page: params[:page], per_page: @params[:perpage]
			else
				@results = Searchkick.search(keyword, where:options, index_name: [Profile.searchkick_index.name, Classifieds.searchkick_index.name], smart_aggs: false, match: :word_start,  misspellings: {edit_distance: 2}, aggs: {price: {ranges: price_ranges}, tags:{}, brandname:{}, category:{}, category_parent:{},services:{}}, highlight: true, suggest: true, boost_where: {featured: {value: 1, factor: 100}, profeatured: {value: 1, factor: 50}}, smart_aggs: false, match: :word_start,  page: params[:page], per_page: @params[:perpage])
			end

		  	@aggs = @results.aggs
		  	@aggKeys = @aggs.keys	

		end

	  @json = Hash.new

	  #@json = @results.to_json

	end

end
