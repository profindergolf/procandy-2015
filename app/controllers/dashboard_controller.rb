class DashboardController < ApplicationController

	before_action :authenticate_user!

	def index
		session[:dashboardtour] = true

		@user = current_user

		@ad_view_analytics = Analytic.where(:profiles_id => current_user.id, :analytic_type => 2)
		#@ad_view_daily_stats = @ad_view_analytics.group_by { |t| t.created_at.beginning_of_day } 
		#@ad_view_weekly_stats = @ad_view_analytics.group_by { |t| t.created_at.beginning_of_week }
		@ad_view_monthly_stats = @ad_view_analytics.group_by { |t| t.created_at.beginning_of_month.strftime('%m%Y')  }
		@ad_unique_views = @ad_view_analytics.group_by { |t| t.identifier }


		if defined?(@ad_view_analytics.properties)
			@advert_keywords = ≈.group_by{ |t| t.properties['searchQuery'] }
			if @advert_keywords
				@advert_keywords.sort.take(4)
			end
			@mostpopadvert = @advert_keywords.shift
		end

		@ad_sales_analytics = Analytic.where(:profiles_id => current_user.id, :analytic_type => 3)
		#@ad_view_daily_stats = @ad_view_analytics.group_by { |t| t.created_at.beginning_of_day } 
		#@ad_view_weekly_stats = @ad_view_analytics.group_by { |t| t.created_at.beginning_of_week }
		@ad_sales_monthly_stats = @ad_sales_analytics.group_by { |t| t.created_at.beginning_of_month.strftime('%m%Y')  }

		@profile_view_analytics = Analytic.where(:profiles_id => current_user.id, :analytic_type => 1)
		#@ad_view_daily_stats = @ad_view_analytics.group_by { |t| t.created_at.beginning_of_day } 
		#@ad_view_weekly_stats = @ad_view_analytics.group_by { |t| t.created_at.beginning_of_week }
		@profile_view_monthly_stats = @profile_view_analytics.group_by { |t| t.created_at.beginning_of_month.strftime('%m%Y')  }

		if defined?(@profile_view_analytics.properties)
			@profile_keywords = @profile_view_analytics.group_by{ |t| t.properties['searchQuery'] }
			if @profile_keywords
				@profile_keywords.sort.take(4)
			end
			@mostpopprofile = @profile_keywords.shift
		end

		@cur_start_month = Date.today.beginning_of_month.strftime('%m%Y')
		@last_start_month = Date.today.ago(1.month).beginning_of_month.strftime('%m%Y')

		puts @ad_view_monthly_stats[@cur_start_month].inspect

		if defined?(@ad_view_monthly_stats[@cur_start_month]) and !@ad_view_monthly_stats[@cur_start_month] == nil
			@whole_month_ad_views = @ad_view_monthly_stats[@cur_start_month].count
		end
		if defined?(@ad_sales_monthly_stats[@cur_start_month]) and !@ad_sales_monthly_stats[@cur_start_month] == nil
			@whole_month_ad_sales = @ad_sales_monthly_stats[@cur_start_month].count
		end
		if defined?(@profile_view_monthly_stats[@cur_start_month]) and !@profile_view_monthly_stats[@cur_start_month] == nil
			@whole_month_profile_views = @profile_view_monthly_stats[@cur_start_month].count
		end

		if @ad_view_monthly_stats[@cur_start_month] != nil
			@monthy_daily_ad_views = @ad_view_monthly_stats[@cur_start_month].group_by { |t| t.created_at.beginning_of_day.strftime('%d%m%Y')  }
		end

		if @ad_sales_monthly_stats[@cur_start_month] != nil
			@monthy_daily_ad_sales = @ad_sales_monthly_stats[@cur_start_month].group_by { |t| t.created_at.beginning_of_day.strftime('%d%m%Y')  }
		end

		if @profile_view_monthly_stats[@cur_start_month] != nil
			@monthy_daily_profile_views = @profile_view_monthly_stats[@cur_start_month].group_by { |t| t.created_at.beginning_of_day.strftime('%d%m%Y')  }
		end

		@this_month_profile_views = @monthy_daily_profile_views

		if @profile_view_monthly_stats[@last_start_month] != nil
			@last_month_profile_views = @profile_view_monthly_stats[@last_start_month].group_by { |t| t.created_at.beginning_of_day.strftime('%d%m%Y')  }
		end

		@classAds = Classifieds.where(user_id: current_user.id).order("created_at desc")

		puts @classAds.inspect
		if !@classAds.nil?
			@classAds = @classAds.order('created_at').page(params[:page]).per(10)

		end
	end

  	protected
	  def authenticate_user!
	    if user_signed_in?
	      super
	    else
	      redirect_to login_path, :notice => 'if you want to add a notice'
	      ## if you want render 404 page
	      ## render :file => File.join(Rails.root, 'public/404'), :formats => [:html], :status => 404, :layout => false
	    end
  	end	

end
