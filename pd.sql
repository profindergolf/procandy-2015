-- MySQL dump 10.16  Distrib 10.1.9-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: procandy_development
-- ------------------------------------------------------
-- Server version	10.1.9-MariaDB-1~trusty

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_admin_comments`
--

DROP TABLE IF EXISTS `active_admin_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_admin_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) DEFAULT NULL,
  `body` text,
  `resource_id` varchar(255) NOT NULL,
  `resource_type` varchar(255) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `author_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_active_admin_comments_on_namespace` (`namespace`),
  KEY `index_active_admin_comments_on_author_type_and_author_id` (`author_type`,`author_id`),
  KEY `index_active_admin_comments_on_resource_type_and_resource_id` (`resource_type`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_admin_comments`
--

LOCK TABLES `active_admin_comments` WRITE;
/*!40000 ALTER TABLE `active_admin_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_admin_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admin_users_on_email` (`email`),
  UNIQUE KEY `index_admin_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'callum.e.hopkins@gmail.com','$2a$10$14cqNTHSvytoHuidmFLVGOFuXNK7ZcItls.KQLEm01V1u60hGz3ne',NULL,NULL,NULL,5,'2016-01-17 19:45:46','2016-01-17 16:06:45','192.168.0.7','192.168.0.7','0000-00-00 00:00:00','2016-01-17 19:45:46');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ahoy_events`
--

DROP TABLE IF EXISTS `ahoy_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahoy_events` (
  `id` binary(16) NOT NULL,
  `visit_id` binary(16) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `properties` text,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ahoy_events_on_visit_id` (`visit_id`),
  KEY `index_ahoy_events_on_user_id` (`user_id`),
  KEY `index_ahoy_events_on_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahoy_events`
--

LOCK TABLES `ahoy_events` WRITE;
/*!40000 ALTER TABLE `ahoy_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ahoy_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brandrels`
--

DROP TABLE IF EXISTS `brandrels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brandrels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classifieds_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brandrels`
--

LOCK TABLES `brandrels` WRITE;
/*!40000 ALTER TABLE `brandrels` DISABLE KEYS */;
INSERT INTO `brandrels` VALUES (6,3,43,'2016-01-02 19:15:06','2016-01-02 19:15:06'),(9,4,68,'2016-01-02 23:29:33','2016-01-02 23:29:33'),(22,5,16,'2016-01-08 15:17:30','2016-01-08 15:17:30'),(23,2,1,'2016-01-19 20:02:02','2016-01-19 20:02:02');
/*!40000 ALTER TABLE `brandrels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandname` varchar(255) NOT NULL DEFAULT '',
  `desc` longtext,
  `tax` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Adidas',NULL,'adidas','2015-12-14 19:27:33','2015-12-14 19:27:33'),(2,'Adams Golf',NULL,'adamsgolf','2015-12-14 19:27:33','2015-12-14 19:27:33'),(3,'Antigua',NULL,'antigua','2015-12-14 19:27:33','2015-12-14 19:27:33'),(4,'Bridgestone Golf',NULL,'bridgestonegolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(5,'Callaway Golf',NULL,'callawaygolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(6,'Cleveland Golf',NULL,'clevelandgolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(7,'Cobra Golf',NULL,'cobragolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(8,'Cutter and Buck',NULL,'cutterandbuck','2015-12-14 19:27:34','2015-12-14 19:27:34'),(9,'Element 21',NULL,'element21','2015-12-14 19:27:34','2015-12-14 19:27:34'),(10,'John Letters',NULL,'johnletters','2015-12-14 19:27:34','2015-12-14 19:27:34'),(11,'Lamkin Grips',NULL,'lamkingrips','2015-12-14 19:27:34','2015-12-14 19:27:34'),(12,'Loudmouth Golf',NULL,'loudmouthgolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(13,'MacGregor Golf',NULL,'macGregorgolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(14,'Maxfli',NULL,'maxfli','2015-12-14 19:27:34','2015-12-14 19:27:34'),(15,'Mizuno',NULL,'mizuno','2015-12-14 19:27:34','2015-12-14 19:27:34'),(16,'Nike Golf',NULL,'nikegolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(17,'Ping',NULL,'ping','2015-12-14 19:27:34','2015-12-14 19:27:34'),(18,'PowaKaddy',NULL,'powaladdy','2015-12-14 19:27:34','2015-12-14 19:27:34'),(19,'Ben Sayers',NULL,'bensayers','2015-12-14 19:27:34','2015-12-14 19:27:34'),(20,'Slazenger',NULL,'slazenger','2015-12-14 19:27:34','2015-12-14 19:27:34'),(21,'Titleist',NULL,'titleist','2015-12-14 19:27:34','2015-12-14 19:27:34'),(22,'Top Flite Golf',NULL,'topflitegolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(23,'Wilson Staff',NULL,'wilsonstaff','2015-12-14 19:27:34','2015-12-14 19:27:34'),(24,'Yonex',NULL,'yonex','2015-12-14 19:27:34','2015-12-14 19:27:34'),(25,'Abacus',NULL,'abacus','2015-12-14 19:27:34','2015-12-14 19:27:34'),(26,'Masters',NULL,'masters','2015-12-14 19:27:34','2015-12-14 19:27:34'),(27,'MD Golf',NULL,'mdgolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(28,'Ashworth',NULL,'ashworth','2015-12-14 19:27:34','2015-12-14 19:27:34'),(29,'Batteries Direct',NULL,'batteriesdirect','2015-12-14 19:27:34','2015-12-14 19:27:34'),(30,'Benross',NULL,'benross','2015-12-14 19:27:34','2015-12-14 19:27:34'),(31,'Brand Fusion',NULL,'brandfusion','2015-12-14 19:27:34','2015-12-14 19:27:34'),(32,'Oakley',NULL,'oakley','2015-12-14 19:27:34','2015-12-14 19:27:34'),(33,'Bushnell Golf',NULL,'bushnellgolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(34,'Daily Sports',NULL,'dailysports','2015-12-14 19:27:34','2015-12-14 19:27:34'),(35,'Dwyers and Co',NULL,'dwyersandco','2015-12-14 19:27:34','2015-12-14 19:27:34'),(36,'Ecco',NULL,'ecco','2015-12-14 19:27:34','2015-12-14 19:27:34'),(37,'Footjoy',NULL,'footjoy','2015-12-14 19:27:34','2015-12-14 19:27:34'),(38,'Calvin Green',NULL,'calvingreen','2015-12-14 19:27:34','2015-12-14 19:27:34'),(39,'Glenbrae',NULL,'glenbrae','2015-12-14 19:27:34','2015-12-14 19:27:34'),(40,'Glenmuir',NULL,'glenmuir','2015-12-14 19:27:34','2015-12-14 19:27:34'),(41,'Golfbuddy',NULL,'golfbuddy','2015-12-14 19:27:34','2015-12-14 19:27:34'),(42,'Golfino',NULL,'golfino','2015-12-14 19:27:34','2015-12-14 19:27:34'),(43,'Golfstream',NULL,'golfstream','2015-12-14 19:27:34','2015-12-14 19:27:34'),(44,'Green Lamb',NULL,'greenlamb','2015-12-14 19:27:34','2015-12-14 19:27:34'),(45,'IJP Design',NULL,'ijpdesign','2015-12-14 19:27:34','2015-12-14 19:27:34'),(46,'J.Lindeberg',NULL,'jlindeberg','2015-12-14 19:27:34','2015-12-14 19:27:34'),(47,'Lyle and Scott',NULL,'lyleandscott','2015-12-14 19:27:34','2015-12-14 19:27:34'),(48,'Marbas',NULL,'marbas','2015-12-14 19:27:34','2015-12-14 19:27:34'),(49,'Murraygolf',NULL,'murraygolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(50,'Nikon',NULL,'nikon','2015-12-14 19:27:34','2015-12-14 19:27:34'),(51,'Oscar Jacobson',NULL,'oscarjacobson','2015-12-14 19:27:34','2015-12-14 19:27:34'),(52,'Peter Millar',NULL,'petermillar','2015-12-14 19:27:34','2015-12-14 19:27:34'),(53,'Peter Scott',NULL,'peterscott','2015-12-14 19:27:34','2015-12-14 19:27:34'),(54,'Pro Quip',NULL,'proquip','2015-12-14 19:27:34','2015-12-14 19:27:34'),(55,'Puma',NULL,'puma','2015-12-14 19:27:34','2015-12-14 19:27:34'),(56,'Rohnisch',NULL,'rohnisch','2015-12-14 19:27:34','2015-12-14 19:27:34'),(57,'Skycaddie',NULL,'skycaddie','2015-12-14 19:27:34','2015-12-14 19:27:34'),(58,'Snake Eyes',NULL,'snakeeyes','2015-12-14 19:27:34','2015-12-14 19:27:34'),(59,'Srixon',NULL,'srixon','2015-12-14 19:27:34','2015-12-14 19:27:34'),(60,'Stromberg',NULL,'stromberg','2015-12-14 19:27:34','2015-12-14 19:27:34'),(61,'Sunderland',NULL,'sunderland','2015-12-14 19:27:34','2015-12-14 19:27:34'),(62,'Sundog',NULL,'sundog','2015-12-14 19:27:34','2015-12-14 19:27:34'),(63,'Sunice',NULL,'sunice','2015-12-14 19:27:34','2015-12-14 19:27:34'),(64,'TaylorMade',NULL,'taylormade','2015-12-14 19:27:34','2015-12-14 19:27:34'),(65,'Tommy Hilfiger',NULL,'tommyhilfiger','2015-12-14 19:27:34','2015-12-14 19:27:34'),(66,'Under Armour',NULL,'underarmour','2015-12-14 19:27:34','2015-12-14 19:27:34'),(67,'UK Kids Golf',NULL,'ukkidsgolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(68,'Wilson Golf',NULL,'wilsongolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(69,'Yes Golf',NULL,'yesgolf','2015-12-14 19:27:34','2015-12-14 19:27:34'),(70,'Eletric Golf Batteries',NULL,'eletricgolfbatteries','2015-12-14 19:27:35','2015-12-14 19:27:35');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classadtags`
--

DROP TABLE IF EXISTS `classadtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classadtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classifieds_id` int(11) NOT NULL DEFAULT '0',
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classadtags`
--

LOCK TABLES `classadtags` WRITE;
/*!40000 ALTER TABLE `classadtags` DISABLE KEYS */;
INSERT INTO `classadtags` VALUES (8,3,2,'2016-01-02 19:15:06','2016-01-02 19:15:06'),(11,4,5,'2016-01-02 23:29:33','2016-01-02 23:29:33'),(24,5,1,'2016-01-08 15:17:30','2016-01-08 15:17:30'),(25,2,1,'2016-01-19 20:02:02','2016-01-19 20:02:02');
/*!40000 ALTER TABLE `classadtags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classcats`
--

DROP TABLE IF EXISTS `classcats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classcats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classname` varchar(255) NOT NULL DEFAULT '',
  `tax` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `parenttype` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classcats`
--

LOCK TABLES `classcats` WRITE;
/*!40000 ALTER TABLE `classcats` DISABLE KEYS */;
INSERT INTO `classcats` VALUES (1,'Stand Bags','standbags','2015-12-14 19:27:41','2015-12-14 19:27:41',2),(2,'Cart Bags','cartbags','2015-12-14 19:27:41','2015-12-14 19:27:41',2),(3,'Pencil Bags','pencilbags','2015-12-14 19:27:41','2015-12-14 19:27:41',2),(4,'New Balls','newballs','2015-12-14 19:27:41','2015-12-14 19:27:41',8),(5,'Lake Balls','lakeballs','2015-12-14 19:27:41','2015-12-14 19:27:41',8),(6,'Pull Cart and Trolleys','pullcartandtrolleys','2015-12-14 19:27:42','2015-12-14 19:27:42',3),(7,'Eletric Trolleys','eletrictrolleys','2015-12-14 19:27:42','2015-12-14 19:27:42',3),(8,'Small Golves','smallgolves','2015-12-14 19:27:42','2015-12-14 19:27:42',4),(9,'Medium Gloves','mediumgloves','2015-12-14 19:27:42','2015-12-14 19:27:42',4),(10,'Medium/Large Gloves','mediumlargegloves','2015-12-14 19:27:42','2015-12-14 19:27:42',4),(11,'Large Gloves','largegloves','2015-12-14 19:27:42','2015-12-14 19:27:42',4),(12,'Right Hand Gloves','righthandgloves','2015-12-14 19:27:42','2015-12-14 19:27:42',4),(13,'Travel Covers','travelcovers','2015-12-14 19:27:42','2015-12-14 19:27:42',4),(14,'Shoe Bags','shoebags','2015-12-14 19:27:42','2015-12-14 19:27:42',5),(15,'Luggage','luggage','2015-12-14 19:27:42','2015-12-14 19:27:42',5),(16,'Umbrellas','umbrellas','2015-12-14 19:27:42','2015-12-14 19:27:42',6),(17,'Towels','towels','2015-12-14 19:27:42','2015-12-14 19:27:42',6),(18,'Golf Tees','golftees','2015-12-14 19:27:42','2015-12-14 19:27:42',6),(19,'Headcovers','headcovers','2015-12-14 19:27:42','2015-12-14 19:27:42',6),(20,'Headwear','headwear','2015-12-14 19:27:42','2015-12-14 19:27:42',6),(21,'Miscellaneous','misc','2015-12-14 19:27:42','2015-12-14 19:27:42',7),(22,'Drivers','drivers','2015-12-14 19:27:42','2015-12-14 19:27:42',1),(23,'Fairway Woods','fairwayWoods','2015-12-14 19:27:42','2015-12-14 19:27:42',1),(24,'Iron Sets','ironSets','2015-12-14 19:27:42','2015-12-14 19:27:42',1),(25,'Wedges','wedges','2015-12-14 19:27:42','2015-12-14 19:27:42',1),(26,'Putters','putters','2015-12-14 19:27:42','2015-12-14 19:27:42',1),(27,'Hybrids','hybrids','2015-12-14 19:27:43','2015-12-14 19:27:43',1);
/*!40000 ALTER TABLE `classcats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classifieds`
--

DROP TABLE IF EXISTS `classifieds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classifieds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `adtitle` varchar(255) NOT NULL DEFAULT '',
  `price` decimal(8,2) DEFAULT NULL,
  `description` longtext,
  `image` int(11) DEFAULT '0',
  `vendor` int(11) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `adtype` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `cat` int(11) DEFAULT '0',
  `remote_url` longtext,
  `external_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classifieds`
--

LOCK TABLES `classifieds` WRITE;
/*!40000 ALTER TABLE `classifieds` DISABLE KEYS */;
INSERT INTO `classifieds` VALUES (2,1,'New Jack Nicklaus Golf Full Set 11 Clubs 7 Irons + Hybrid Driver 3 Wood & Putter',109.95,'New Jack Nicklaus Golf Full Set 11 Clubs 7 Irons + Hybrid Driver 3 Wood &amp; PutterDept70 Exclusive | RRP £349.99 | Free Next Day DeliveryRRP £349.99Features: Exclusive set to Department70Limited edition gunmetal finishJack Nicklaus quality guaranteedStainless steel headsHigh launch rescue clubLow torque graphite shafted woodsQuality Jack Nicklaus rubber gripNo head covers includedOver 70% off RRP!!',3,1,'http://www.ebay.co.uk/itm/New-Jack-Nicklaus-Golf-Full-Set-11-Clubs-7-Irons-Hybrid-Driver-3-Wood-Putter-/301672485604?var=&hash=item463d14d2e4:m:mxYtb93bIj7lGJUjc3PRdeQ',1,1,'2015-12-30 13:58:49','2016-01-19 20:02:02',24,'http://i.ebayimg.com/00/s/MTYwMFgxNDA3/z/6~AAAOSw3ydVjWM~/$_1.JPG?set_id=880000500F',''),(3,1,'New RAM Golf Qub3 Shuttle 10.5 Deg Driver 3 & 5 Wood Graphite Grafalloy Club Set',59.95,'New RAM Golf Qub3 Shuttle 10.5 Deg Driver 3 &amp; 5 Wood Graphite Grafalloy Club SetDept70 | Genuine RAM | RRP £299.99!! | Free + Fast P&amp;PRRP £299.99Features: Exclusive set to Department70Limited edition square head technologyUpgraded Grafalloy® pro lite shaftRAM quality guaranteedTitanium driver headsStainless steel wood headsQuality rubber gripHead covers includedOver 70% off RRP!!',5,2,'http://www.ebay.co.uk/itm/New-RAM-Golf-Qub3-Shuttle-10-5-Deg-Driver-3-5-Wood-Graphite-Grafalloy-Club-Set/301568977591?',1,4,'2015-12-30 13:59:29','2016-01-02 19:15:06',11,'http://i.ebayimg.com/00/s/MTA2MFgxNjAw/z/sX0AAOSwI-BWOfJP/$_1.JPG?set_id=880000500F',NULL),(4,1,'Ram Fxi Tour Driver Grafalloy Prolight Reg Shaft Choose Loft Tri Technology ',29.99,'Ram Fxi Driver Tri Tech lofts of 12 and 14 degree head cover not supplied',7,2,'http://www.ebay.co.uk/itm/Ram-Fxi-Tour-Driver-Grafalloy-Prolight-Reg-Shaft-Choose-Loft-Tri-Technology/252132401043?',1,1,'2016-01-02 23:24:01','2016-01-02 23:29:33',22,'http://i.ebayimg.com/00/s/MTI1M1g4ODc=/z/qDcAAOSwAYtWJQKi/$_12.JPG?set_id=880000500F','252132401043'),(5,1,'tets product 1',100.00,'this is just a test    ',8,1,'',1,1,'2016-01-06 20:36:31','2016-01-08 15:17:30',22,'','');
/*!40000 ALTER TABLE `classifieds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classtypes`
--

DROP TABLE IF EXISTS `classtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(255) NOT NULL DEFAULT '',
  `tax` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classtypes`
--

LOCK TABLES `classtypes` WRITE;
/*!40000 ALTER TABLE `classtypes` DISABLE KEYS */;
INSERT INTO `classtypes` VALUES (1,'Clubs','clubs','/clubs','2015-12-14 19:34:28','2015-12-14 19:34:28'),(2,'Bags','bags','/bags','2015-12-14 19:34:28','2015-12-14 19:34:28'),(3,'Trolleys','trolleys','/trolleys','2015-12-14 19:34:28','2015-12-14 19:34:28'),(4,'Gloves','gloves','/gloves','2015-12-14 19:34:28','2015-12-14 19:34:28'),(5,'Travel Gear','travel','/travel','2015-12-14 19:34:28','2015-12-14 19:34:28'),(6,'Accessories','accessories','/accessories','2015-12-14 19:34:28','2015-12-14 19:34:28'),(7,'Miscellaneous','miscellaneous','/miscellaneous','2015-12-14 19:34:28','2015-12-14 19:34:28'),(8,'Balls','balls','/balls','2015-12-14 19:34:37','2015-12-14 19:34:37');
/*!40000 ALTER TABLE `classtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `protype` varchar(255) NOT NULL DEFAULT 'image',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `theimage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_images_on_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (9,1,'avatar','2016-01-19 22:06:38','2016-01-19 22:06:38','file.png'),(10,1,'avatar','2016-01-19 22:09:33','2016-01-19 22:09:33','file.png'),(11,1,'avatar','2016-01-19 22:11:50','2016-01-19 22:11:50','file.png'),(12,1,'avatar','2016-01-19 22:15:35','2016-01-19 22:15:35','file.png'),(13,1,'avatar','2016-01-19 22:34:10','2016-01-19 22:34:10','file.png'),(14,1,'avatar','2016-01-19 22:40:46','2016-01-19 22:40:46','file.png');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailboxer_conversation_opt_outs`
--

DROP TABLE IF EXISTS `mailboxer_conversation_opt_outs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailboxer_conversation_opt_outs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unsubscriber_id` int(11) DEFAULT NULL,
  `unsubscriber_type` varchar(255) DEFAULT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type` (`unsubscriber_id`,`unsubscriber_type`),
  KEY `index_mailboxer_conversation_opt_outs_on_conversation_id` (`conversation_id`),
  CONSTRAINT `mb_opt_outs_on_conversations_id` FOREIGN KEY (`conversation_id`) REFERENCES `mailboxer_conversations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailboxer_conversation_opt_outs`
--

LOCK TABLES `mailboxer_conversation_opt_outs` WRITE;
/*!40000 ALTER TABLE `mailboxer_conversation_opt_outs` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailboxer_conversation_opt_outs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailboxer_conversations`
--

DROP TABLE IF EXISTS `mailboxer_conversations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailboxer_conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailboxer_conversations`
--

LOCK TABLES `mailboxer_conversations` WRITE;
/*!40000 ALTER TABLE `mailboxer_conversations` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailboxer_conversations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailboxer_notifications`
--

DROP TABLE IF EXISTS `mailboxer_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailboxer_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `body` text,
  `subject` varchar(255) DEFAULT '',
  `sender_id` int(11) DEFAULT NULL,
  `sender_type` varchar(255) DEFAULT NULL,
  `conversation_id` int(11) DEFAULT NULL,
  `draft` tinyint(1) DEFAULT '0',
  `notification_code` varchar(255) DEFAULT NULL,
  `notified_object_id` int(11) DEFAULT NULL,
  `notified_object_type` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `global` tinyint(1) DEFAULT '0',
  `expires` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_mailboxer_notifications_on_conversation_id` (`conversation_id`),
  KEY `index_mailboxer_notifications_on_type` (`type`),
  KEY `index_mailboxer_notifications_on_sender_id_and_sender_type` (`sender_id`,`sender_type`),
  KEY `index_mailboxer_notifications_on_notified_object_id_and_type` (`notified_object_id`,`notified_object_type`),
  CONSTRAINT `notifications_on_conversation_id` FOREIGN KEY (`conversation_id`) REFERENCES `mailboxer_conversations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailboxer_notifications`
--

LOCK TABLES `mailboxer_notifications` WRITE;
/*!40000 ALTER TABLE `mailboxer_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailboxer_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailboxer_receipts`
--

DROP TABLE IF EXISTS `mailboxer_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailboxer_receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receiver_id` int(11) DEFAULT NULL,
  `receiver_type` varchar(255) DEFAULT NULL,
  `notification_id` int(11) NOT NULL,
  `is_read` tinyint(1) DEFAULT '0',
  `trashed` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(25) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_mailboxer_receipts_on_notification_id` (`notification_id`),
  KEY `index_mailboxer_receipts_on_receiver_id_and_receiver_type` (`receiver_id`,`receiver_type`),
  CONSTRAINT `receipts_on_notification_id` FOREIGN KEY (`notification_id`) REFERENCES `mailboxer_notifications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailboxer_receipts`
--

LOCK TABLES `mailboxer_receipts` WRITE;
/*!40000 ALTER TABLE `mailboxer_receipts` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailboxer_receipts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plans`
--

DROP TABLE IF EXISTS `plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) DEFAULT NULL,
  `gold` tinyint(1) NOT NULL,
  `ad_archive` tinyint(1) NOT NULL,
  `price` decimal(8,2) DEFAULT '10.00',
  `ads` int(11) NOT NULL DEFAULT '3',
  `service` int(11) NOT NULL DEFAULT '3',
  `photos` int(11) NOT NULL DEFAULT '3',
  `homepage_ads` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `local_items` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plans`
--

LOCK TABLES `plans` WRITE;
/*!40000 ALTER TABLE `plans` DISABLE KEYS */;
INSERT INTO `plans` VALUES (1,'Practice Range',0,0,0.00,3,3,3,0,'2015-12-29 09:54:47','2015-12-29 10:02:27',0),(2,'Gold Certified',1,0,19.99,3,3,3,0,'2015-12-29 10:00:20','2016-01-17 19:47:53',0),(3,'Red Tee',1,1,49.99,5,5,5,1,'2015-12-29 10:01:52','2016-01-17 19:47:24',1),(4,'Yellow Tee',1,1,99.99,10,99,10,5,'2015-12-29 10:04:26','2016-01-17 19:47:43',1),(5,'White Tee',1,1,199.99,25,99,20,25,'2015-12-29 10:08:16','2016-01-17 19:47:46',1);
/*!40000 ALTER TABLE `plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `bio` varchar(255) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `golfclub` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT '1',
  `phone` varchar(255) DEFAULT NULL,
  `wizard` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '1',
  `stripe_id` varchar(255) DEFAULT NULL,
  `merchant_setup` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_profiles_on_id` (`id`),
  KEY `index_profiles_on_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,'Callum Hopkins','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed \r\npurus varius, maximus tellus quis, tincidunt tortor. Duis in felis \r\nsollicitudin, pharetra quam nec, pellentesque est. ',NULL,NULL,'2015-12-14 19:16:22','2016-01-20 22:16:38',57.2776,-2.38349,'ab51 3yp',1,1,'Garioch Golf Club',3,'07840033841',0,1,'cus_7ec2PpN9C35m3w','1');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proservs`
--

DROP TABLE IF EXISTS `proservs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proservs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL DEFAULT '0',
  `services_id` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proservs`
--

LOCK TABLES `proservs` WRITE;
/*!40000 ALTER TABLE `proservs` DISABLE KEYS */;
INSERT INTO `proservs` VALUES (18,1,6,NULL,NULL,'2016-01-03 20:51:55','2016-01-03 20:51:55'),(19,1,8,NULL,NULL,'2016-01-03 20:51:55','2016-01-03 20:51:55'),(21,1,10,NULL,NULL,'2016-01-03 20:51:55','2016-01-03 20:51:55');
/*!40000 ALTER TABLE `proservs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20151102215149'),('20151107111430'),('20151107135106'),('20151107135116'),('20151107142015'),('20151108201105'),('20151108205247'),('20151108215102'),('20151111215359'),('20151111215360'),('20151111215361'),('20151114142609'),('20151114143938'),('20151115131006'),('20151115143301'),('20151115170038'),('20151115173313'),('20151115200518'),('20151115210951'),('20151115223305'),('20151118165812'),('20151119001324'),('20151119094908'),('20151119095008'),('220151119095108'),('220151119095109'),('220151119095110'),('220151119095111'),('220151119095112'),('220151119095113'),('220151119095114'),('220151119095115'),('220151119095116'),('220151119095117'),('220151119095118'),('220151119095119'),('220151119095120'),('220151119095121'),('220151119095122'),('220151119095123'),('220151119095124'),('220151119095125'),('220151119095126'),('220151119095127'),('220151119095128'),('220151119095129'),('220151119095130'),('220151119095131'),('220151119095132'),('220151119095133'),('220151119095134'),('220151119095135'),('220151119095136');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servtitle` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Golf Lessons','2015-12-14 20:57:34','2015-12-14 20:57:34'),(2,'Buggy - Cart Hire','2015-12-14 20:57:34','2015-12-14 20:57:34'),(3,'Custom Fitting','2015-12-14 20:57:34','2015-12-14 20:57:34'),(4,'Trackman - Flightscope','2015-12-14 20:57:34','2015-12-14 20:57:34'),(5,'Club Repair','2015-12-14 20:57:34','2015-12-14 20:57:34'),(6,'Pro Retail Shop','2015-12-14 20:57:34','2015-12-14 20:57:34'),(7,'After Dinner Speaking','2015-12-14 20:57:34','2015-12-14 20:57:34'),(8,'Driving Range','2015-12-14 20:57:34','2015-12-14 20:57:34'),(9,'Pratice Facilities','2015-12-14 20:57:34','2015-12-14 20:57:34'),(10,'Restaurant & Bar','2015-12-14 20:57:34','2015-12-14 20:57:34');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagname` varchar(255) NOT NULL DEFAULT '',
  `tagtype` int(11) NOT NULL DEFAULT '9',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'Mens',0,'2015-12-14 19:42:41','2015-12-14 19:42:41'),(2,'Ladies',0,'2015-12-14 19:42:41','2015-12-14 19:42:41'),(3,'Junior',0,'2015-12-14 19:42:41','2015-12-14 19:42:41'),(4,'Left Handed',0,'2015-12-14 19:42:41','2015-12-14 19:42:41'),(5,'Right Handed',0,'2015-12-14 19:42:41','2015-12-14 19:42:41'),(6,'Pre-owned',0,'2015-12-14 19:42:41','2015-12-14 19:42:41'),(7,'New',0,'2015-12-14 19:42:42','2015-12-14 19:42:42');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `publishable_key` varchar(255) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `access_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'callum.e.hopkins@gmail.com','$2a$10$G.R2wlye2tDXMCZFV4/KjO86.mczvyV6InJzNusKwQZDYu9SFkPFq','f5c01f8aa8de56b956b3a30f05485cf80d17f521aa40b3a4d3c7982263f1cefb','2016-01-05 15:07:25',NULL,27,'2016-01-20 22:17:05','2016-01-20 22:00:07','192.168.0.7','192.168.0.7','2015-12-14 19:16:22','2016-01-20 22:17:05','pk_test_TSBuJ785JAXGCHeZQMlcOVHA','stripe_connect','acct_1kR2XHdfdWHOtSEG7IBb','sk_test_8CT6dCz863MuF3z6PqkgZR9Y');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `id` binary(16) NOT NULL,
  `visitor_id` binary(16) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `user_agent` text,
  `referrer` text,
  `landing_page` text,
  `user_id` int(11) DEFAULT NULL,
  `referring_domain` varchar(255) DEFAULT NULL,
  `search_keyword` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `screen_height` int(11) DEFAULT NULL,
  `screen_width` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `latitude` decimal(10,0) DEFAULT NULL,
  `longitude` decimal(10,0) DEFAULT NULL,
  `utm_source` varchar(255) DEFAULT NULL,
  `utm_medium` varchar(255) DEFAULT NULL,
  `utm_term` varchar(255) DEFAULT NULL,
  `utm_content` varchar(255) DEFAULT NULL,
  `utm_campaign` varchar(255) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_visits_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` VALUES ('\n1����JK��_fE��','�۾JE�>\"x����','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',NULL,'http://local.profinder.golf:3000/users/sign_in',1,NULL,NULL,'Chrome','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-16 13:50:53'),('�S\Z�lM6���a5','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-17 14:28:44'),('$�X�ڈF^�#�\r�','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-16 11:23:45'),('*`����L���6M_{i�','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-20 18:27:46'),('Z��ӝG������p~','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-11 19:24:10'),('e���\rOl�������','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',NULL,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-04 21:07:39'),('l����\nI�����FF�','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/search',NULL,NULL,NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-10 11:29:23'),('�����F6��m�v�','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-19 18:58:49'),('���JE���r�\n�I','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/search',NULL,NULL,NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-09 11:51:03'),('��R?�Eϭ�����T/','�۾JE�>\"x����','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36','http://local.profinder.golf:3000/users/sign_in','http://local.profinder.golf:3000/dashboard',1,'local.profinder.golf',NULL,'Chrome','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-06 21:17:32'),('��x�C�\r6.��D','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',NULL,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-04 12:24:05'),('�;����BY�?hjԅf','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0','http://local.profinder.golf:3000/users/sign_in','http://local.profinder.golf:3000/dashboard',1,'local.profinder.golf',NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-06 20:14:30'),('��wk�D2��-�3=6\r','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-08 15:11:50'),('֎�7)�Fփ��R�+�','��R\n\\@����g�','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-20 22:00:00'),('�֑�?M��v�h�','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-05 09:49:24'),('�q��dBH����\Z��\0�','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-13 19:57:12'),('�����Eu��(=\'/@','���#KI�Y�aS�>','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',1,NULL,NULL,'Firefox','Mac OS X','Desktop',900,1440,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-18 17:59:38'),('��<�>D��;�y�Ug','��U���Dv��c��=�A','192.168.0.7','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:44.0) Gecko/20100101 Firefox/44.0',NULL,'http://local.profinder.golf:3000/',NULL,NULL,NULL,'Firefox','Mac OS X','Desktop',1050,1680,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2016-01-17 14:28:32');
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-23  9:03:53
