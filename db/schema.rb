# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 220151119095161) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "addons", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "highlight",                                                     null: false
    t.boolean  "featured",                                                      null: false
    t.boolean  "flashsale",                                                     null: false
    t.decimal  "price",                  precision: 8, scale: 2, default: 10.0
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.string   "desc",       limit: 255,                         default: "",   null: false
  end

  create_table "adextraimgs", force: :cascade do |t|
    t.integer  "classifieds_id", limit: 4, default: 0, null: false
    t.integer  "images_id",      limit: 4, default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "advertaddons", force: :cascade do |t|
    t.integer  "classifieds_id", limit: 4, default: 0, null: false
    t.integer  "addons_id",      limit: 4, default: 0, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "analytics", force: :cascade do |t|
    t.integer  "analytic_type",  limit: 4
    t.string   "identifier",     limit: 255
    t.integer  "profiles_id",    limit: 4
    t.integer  "classifieds_id", limit: 4
    t.string   "properties",     limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "brandrels", force: :cascade do |t|
    t.integer  "classifieds_id", limit: 4, default: 0, null: false
    t.integer  "brand_id",       limit: 4, default: 0, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "brands", force: :cascade do |t|
    t.string   "brandname",  limit: 255,        default: "", null: false
    t.text     "desc",       limit: 4294967295
    t.string   "tax",        limit: 255,        default: "", null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "classadtags", force: :cascade do |t|
    t.integer  "classifieds_id", limit: 4, default: 0, null: false
    t.integer  "tag_id",         limit: 4, default: 0, null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "classcats", force: :cascade do |t|
    t.string   "classname",  limit: 255, default: "", null: false
    t.string   "tax",        limit: 255, default: "", null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "parenttype", limit: 4,   default: 0
  end

  create_table "classifieds", force: :cascade do |t|
    t.integer  "user_id",     limit: 4,                                  default: 0,  null: false
    t.string   "adtitle",     limit: 255,                                default: "", null: false
    t.decimal  "price",                          precision: 8, scale: 2
    t.text     "description", limit: 4294967295
    t.integer  "image",       limit: 4,                                  default: 0
    t.integer  "vendor",      limit: 4,                                  default: 1,  null: false
    t.string   "url",         limit: 255
    t.integer  "status",      limit: 4,                                  default: 1
    t.integer  "adtype",      limit: 4,                                  default: 1,  null: false
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.integer  "cat",         limit: 4,                                  default: 0
    t.text     "remote_url",  limit: 4294967295
    t.string   "external_id", limit: 255
    t.integer  "adaddon",     limit: 4
    t.datetime "enddate"
  end

  create_table "classtypes", force: :cascade do |t|
    t.string   "typename",   limit: 255, default: "", null: false
    t.string   "tax",        limit: 255, default: "", null: false
    t.string   "url",        limit: 255, default: ""
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "images", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,          default: 0,       null: false
    t.string   "protype",    limit: 255,        default: "image", null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.text     "theimage",   limit: 4294967295
  end

  add_index "images", ["id"], name: "index_images_on_id", unique: true, using: :btree

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id",   limit: 4
    t.string  "unsubscriber_type", limit: 255
    t.integer "conversation_id",   limit: 4
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    limit: 255, default: ""
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type",                 limit: 255
    t.text     "body",                 limit: 65535
    t.string   "subject",              limit: 255,   default: ""
    t.integer  "sender_id",            limit: 4
    t.string   "sender_type",          limit: 255
    t.integer  "conversation_id",      limit: 4
    t.boolean  "draft",                              default: false
    t.string   "notification_code",    limit: 255
    t.integer  "notified_object_id",   limit: 4
    t.string   "notified_object_type", limit: 255
    t.string   "attachment",           limit: 255
    t.datetime "updated_at",                                         null: false
    t.datetime "created_at",                                         null: false
    t.boolean  "global",                             default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id",     limit: 4
    t.string   "receiver_type",   limit: 255
    t.integer  "notification_id", limit: 4,                   null: false
    t.boolean  "is_read",                     default: false
    t.boolean  "trashed",                     default: false
    t.boolean  "deleted",                     default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "plans", force: :cascade do |t|
    t.string   "namespace",    limit: 255
    t.boolean  "gold",                                                             null: false
    t.boolean  "ad_archive",                                                       null: false
    t.decimal  "price",                    precision: 8, scale: 2, default: 10.0
    t.integer  "ads",          limit: 4,                           default: 3,     null: false
    t.integer  "service",      limit: 4,                           default: 3,     null: false
    t.integer  "photos",       limit: 4,                           default: 3,     null: false
    t.integer  "homepage_ads", limit: 4,                           default: 0,     null: false
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.boolean  "local_items",                                      default: false, null: false
    t.integer  "maxrange",     limit: 4,                           default: 7,     null: false
    t.integer  "credits",      limit: 4,                           default: 0,     null: false
  end

  create_table "profilebrands", force: :cascade do |t|
    t.integer  "profile_id", limit: 4, default: 0, null: false
    t.integer  "brand_id",   limit: 4, default: 0, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "name",           limit: 255,        default: "",   null: false
    t.text     "bio",            limit: 4294967295
    t.datetime "created"
    t.datetime "updated"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.float    "latitude",       limit: 24
    t.float    "longitude",      limit: 24
    t.string   "postcode",       limit: 255
    t.integer  "user_id",        limit: 4
    t.boolean  "featured"
    t.string   "golfclub",       limit: 255
    t.integer  "level",          limit: 4,          default: 1
    t.string   "phone",          limit: 255
    t.boolean  "wizard",                            default: true
    t.integer  "position",       limit: 4,          default: 1
    t.string   "stripe_id",      limit: 255
    t.string   "merchant_setup", limit: 255
    t.integer  "adcredits",      limit: 4,          default: 0,    null: false
    t.string   "fb",             limit: 255
    t.string   "twitter",        limit: 255
    t.string   "youtube",        limit: 255
    t.text     "times",          limit: 4294967295
  end

  add_index "profiles", ["id"], name: "index_profiles_on_id", unique: true, using: :btree
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "profilestatus", force: :cascade do |t|
    t.integer  "profile_id", limit: 4,   default: 0, null: false
    t.text     "status",     limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "proservs", force: :cascade do |t|
    t.integer  "profile_id",  limit: 4, default: 0, null: false
    t.integer  "services_id", limit: 4, default: 0, null: false
    t.datetime "created"
    t.datetime "updated"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "services", force: :cascade do |t|
    t.string   "servtitle",  limit: 255, default: "", null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "solditems", force: :cascade do |t|
    t.integer  "user_id",     limit: 4,                                  default: 0,     null: false
    t.string   "adtitle",     limit: 255,                                default: "",    null: false
    t.decimal  "price",                          precision: 8, scale: 2
    t.text     "description", limit: 4294967295
    t.integer  "image",       limit: 4,                                  default: 0
    t.integer  "vendor",      limit: 4,                                  default: 1,     null: false
    t.string   "url",         limit: 255
    t.integer  "adtype",      limit: 4,                                  default: 1,     null: false
    t.integer  "cat",         limit: 4,                                  default: 0
    t.text     "remote_url",  limit: 4294967295
    t.string   "external_id", limit: 255
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.string   "stripe_id",   limit: 255
    t.boolean  "refund",                                                 default: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "tagname",    limit: 255, default: "", null: false
    t.integer  "tagtype",    limit: 4,   default: 9,  null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "publishable_key",        limit: 255
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.string   "access_code",            limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
end
