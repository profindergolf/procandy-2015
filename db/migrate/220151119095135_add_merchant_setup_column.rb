class AddMerchantSetupColumn < ActiveRecord::Migration
  def change
  	add_column :profiles, :merchant_setup, :string, null: true
  end
end
