class ChangeBrandrelColName < ActiveRecord::Migration
  def change
  	rename_column :brandrels, :classified_id, :classifieds_id
  end
end
