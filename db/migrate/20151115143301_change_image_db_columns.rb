class ChangeImageDbColumns < ActiveRecord::Migration
  def change
  	add_column :images, :theimage, :string
  	remove_column :images, :image, :string
  	remove_column :images, :created, :datetime
  	remove_column :images, :updated, :datetime
  end
end
