class ChangeProfileTypeColumn < ActiveRecord::Migration
  def change
  	rename_column :images, :type, :protype
  end
end
