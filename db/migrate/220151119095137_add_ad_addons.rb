class AddAdAddons < ActiveRecord::Migration
  def change
    create_table :addons do |t|
    	t.string :name
		t.boolean :highlight, null: false
		t.boolean :featured, null: false
		t.boolean :flashsale, null: false
		t.decimal :price, default: 10, precision: 8, scale: 2
		t.timestamps null:false
    end
  end
end
