class CreateTagsRelationshipsTable < ActiveRecord::Migration
  def change
    create_table :classadtags do |t|
    	t.integer :classadid, 	default:0, null:false
    	t.integer :tagid, 			default:0, null:false

    	t.timestamps null: false
    end
  end
end
