class CreateClassifiedsTable < ActiveRecord::Migration
  def change
    create_table :classifieds do |t|
      t.integer :user_id,             default: 0, null: false
      t.string :adtitle,              null: false, default: ""
      t.decimal :price, 			  precision: 8, scale: 2
      t.text :description,			  limit: 4294967295
      t.integer :image,				  default:0
      t.integer :vendor,			  default: 1, null: false
      t.string :url, 				  null: true
      t.integer :status, 			  default: 1
      t.integer :type,				  default: 1, null: false

      t.timestamps null: false
    end
  end
end
