class AddPlanArchiveOptions < ActiveRecord::Migration
  def change
  	add_column :plans, :local_items, :boolean, null: false, default: false
  end
end
