class IncreaseImageCol < ActiveRecord::Migration
  def change
  	change_column :images, :theimage, :text, :limit => 4294967295, :default => nil
  end
end
