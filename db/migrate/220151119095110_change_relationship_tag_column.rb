class ChangeRelationshipTagColumn < ActiveRecord::Migration
  def change
  	rename_column :classadtags, :classified_id, :classifieds_id
  end
end
