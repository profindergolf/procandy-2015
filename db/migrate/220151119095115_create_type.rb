class CreateType < ActiveRecord::Migration
  def change
    create_table :types do |t|
    	t.string :typename, default: "", null: false
    	t.string :tax, default:"", null:false
    	t.string :url, default:""

    	t.timestamps null: false
    end
  end
end
