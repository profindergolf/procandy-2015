class ProfilesAddSocialMediaLinks < ActiveRecord::Migration
  def change
  	add_column :profiles, :fb, :string
  	add_column :profiles, :twitter, :string
  	add_column :profiles, :youtube, :string
  end
end
