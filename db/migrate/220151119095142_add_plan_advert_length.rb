class AddPlanAdvertLength < ActiveRecord::Migration
  def change
  	add_column :plans, :maxrange, :integer, null:false, default:7
  end
end
