class AddCreditsToPlans < ActiveRecord::Migration
  def change
  	  add_column :plans, :credits, :integer, null:false, default:0
  end
end
