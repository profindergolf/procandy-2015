class AddCreditsToProfiles < ActiveRecord::Migration
  def change
  	add_column :profiles, :adcredits, :integer, null:false, default:0
  end
end
