class CreateServicesTable < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.integer :user_id,             default: 0, null: false
      t.string :servtitle,                 null: false, default: ""

      t.timestamps null: false
    end
  end
end
