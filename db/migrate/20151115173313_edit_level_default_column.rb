class EditLevelDefaultColumn < ActiveRecord::Migration
  def change
  	change_column :profiles, :level, :integer, :default => 1
  	add_column :profiles, :position, :integer, :default => 1
  end
end
