class AddCatToClassifieds < ActiveRecord::Migration
  def change
  	add_column :classifieds, :cat, :integer, :default => 0
  end
end
