class ChangeAdaddonsRelColumns < ActiveRecord::Migration
  def change
  	rename_column :advertaddons, :adid, :classifieds_id
  	rename_column :advertaddons, :addonid, :addons_id
  end
end
