class AddServsRelation < ActiveRecord::Migration
  def change
  	remove_column :services, :user_id, :integer
  	remove_column :services, :serviceid, :integer

  	create_table :proservs do |t|
      t.integer :user_id,             default: 0, null: false
      t.string :service_id,            default: 0, null: false
      t.datetime :created
      t.datetime :updated

      t.timestamps null: false
    end
  end
end
