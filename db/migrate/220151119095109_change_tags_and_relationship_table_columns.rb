class ChangeTagsAndRelationshipTableColumns < ActiveRecord::Migration
  def change
  	rename_column :classadtags, :tagid, :tag_id
  	rename_column :classadtags, :classadid, :classified_id
  end
end
