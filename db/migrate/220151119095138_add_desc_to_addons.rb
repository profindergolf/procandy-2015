class AddDescToAddons < ActiveRecord::Migration
  def change
  	add_column :addons, :desc, :string, null: false, default: ""
  end
end
