class CreateTagsTable < ActiveRecord::Migration
  def change
    create_table :tags do |t|
		t.string :tagname, 	default:"", null:false
		t.integer :type, 	default:9, null:false	

    	t.timestamps null: false
    end
  end
end
