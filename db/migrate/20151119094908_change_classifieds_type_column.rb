class ChangeClassifiedsTypeColumn < ActiveRecord::Migration
  def change
  	rename_column :classifieds, :type, :adtype
  end
end
