class AddClassRemoteUrl < ActiveRecord::Migration
  def change
  	add_column :classifieds, :remote_url, :text, :limit => 4294967295
  end
end
