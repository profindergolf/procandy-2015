class ChangeBrandRelColumnsNames < ActiveRecord::Migration
  def change
  	rename_column :brandrel, :brandid, :brand_id
  	rename_column :brandrel, :classadid, :classified_id
  end
end
