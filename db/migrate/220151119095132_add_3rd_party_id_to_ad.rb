class Add3rdPartyIdToAd < ActiveRecord::Migration
  def change
  	add_column :classifieds, :external_id, :string, null: true
  end
end
