class ChangeProstatsColumn < ActiveRecord::Migration
  def change
  	rename_column :profilestatus, :user_id, :profile_id
  	rename_column :profilestatus, :description, :status
  end
end
