class AdvertExtraImages < ActiveRecord::Migration
  def change
    create_table :adextraimgs do |t|
    	t.integer :classifieds_id, 	default:0, null:false
    	t.integer :images_id,  default:0, null:false

    	t.timestamps null: false
    end
  end
end
