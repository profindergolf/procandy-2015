class AddBioDefault < ActiveRecord::Migration
  def change
  	change_column :profiles, :bio, :text, :limit => 4294967295, :null => true
  end
end
