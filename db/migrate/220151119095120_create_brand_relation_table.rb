class CreateBrandRelationTable < ActiveRecord::Migration
  def change
    create_table :brandrel do |t|

    	t.integer :classadid, 	default:0, null:false
    	t.integer :brandid, 	default:0, null:false

    	t.timestamps null: false
    end
  end
end
