class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
    	t.string :brandname, default:"", null:false
    	t.text :desc, limit: 4294967295
    	t.string :tax, default:"", null:false

    	t.timestamps null: false
    end
  end
end
