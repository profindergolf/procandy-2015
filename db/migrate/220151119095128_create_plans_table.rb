class CreatePlansTable < ActiveRecord::Migration
  def change
    create_table :plans do |t|
    	t.string :namespace
		t.boolean :gold, null: false
		t.boolean :ad_archive, null: false
		t.decimal :price, default: 10, precision: 8, scale: 2
		t.integer :ads, default:3, null:false
		t.integer :service, default:3, null:false
		t.integer :photos, default:3, null:false
		t.integer :homepage_ads, default:0, null:false
		t.timestamps null:false
    end
  end
end
