class AddStripeIdToProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :stripe_id, :string, null: false, default: ""
  end
end
