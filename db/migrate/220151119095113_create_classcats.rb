class CreateClasscats < ActiveRecord::Migration
  def change
    create_table :classcats do |t|
    	t.string :classname, default:"", null:false
    	t.string :tax, default: "", null:false

    	t.timestamps null: false
    end
  end
end
