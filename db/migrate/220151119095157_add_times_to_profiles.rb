class AddTimesToProfiles < ActiveRecord::Migration
  def change
  	add_column :profiles, :times, :text, :limit => 4294967295, :default => nil
  	change_column :profiles, :bio, :text, :limit => 4294967295, :default => nil
  end
end
