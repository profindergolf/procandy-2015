class AddProfileColumns < ActiveRecord::Migration
  def change
  	add_column :profiles, :featured, :boolean
  	add_column :profiles, :golfclub, :string
  	add_column :profiles, :level, :integer
  	add_column :profiles, :phone, :string
  end
end
