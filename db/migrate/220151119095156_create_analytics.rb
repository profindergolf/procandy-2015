class CreateAnalytics < ActiveRecord::Migration
  def change
    create_table :analytics do |t|
      t.integer :analytic_type
      t.string :identifier
      t.integer :profiles_id
      t.integer :classifieds_id
      t.string :properties

      t.timestamps null: false
    end
  end
end
