class ChangeTagTypeCol < ActiveRecord::Migration
  def change
  	rename_column :tags, :type, :tagtype
  end
end
