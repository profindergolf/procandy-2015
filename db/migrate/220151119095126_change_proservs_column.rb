class ChangeProservsColumn < ActiveRecord::Migration
  def change
  	rename_column :proservs, :user_id, :profile_id
  	rename_column :proservs, :service_id, :services_id
  end
end
