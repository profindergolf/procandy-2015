class ProStatusUpdates < ActiveRecord::Migration
  def change
     create_table :profilestatus do |t|
    	t.integer :user_id, 	default:0, null:false
       	t.text :description,    limit: 140

    	t.timestamps null: false
    end
  end
end
