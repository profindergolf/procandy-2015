class ChangeStripeIdCol < ActiveRecord::Migration
  def change
  	remove_column :profiles, :stripe_id, :string
  	add_column :profiles, :stripe_id, :string, null: true
  end
end
