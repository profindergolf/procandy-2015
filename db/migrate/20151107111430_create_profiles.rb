class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id,             default: 0, null: false
      t.string :name,                 null: false, default: ""
      t.string :email,                 null: false, default: ""
      t.string :bio,                  null: false, default: ""
      t.datetime :created
      t.datetime :updated

      t.timestamps null: false
    end

    add_index :profiles, :id,         unique: true
    add_index :profiles, :user_id,    unique: true
  end
end
