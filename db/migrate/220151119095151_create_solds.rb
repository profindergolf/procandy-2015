class CreateSolds < ActiveRecord::Migration
  def change
    create_table :solditems do |t|
		t.integer  :user_id,     limit: 4,                                  default: 0,  null: false
		t.string   :adtitle,     limit: 255,                                default: "", null: false
		t.decimal  :price,                          precision: 8, scale: 2
		t.text     :description, limit: 4294967295
		t.integer  :image,       limit: 4,                                  default: 0
		t.integer  :vendor,      limit: 4,                                  default: 1,  null: false
		t.string   :url,         limit: 255
		t.integer  :adtype,      limit: 4,                                  default: 1,  null: false
		t.integer  :cat,         limit: 4,                                  default: 0
		t.text     :remote_url,  limit: 4294967295
		t.string   :external_id, limit: 255

      	t.timestamps null: false
    end
  end
end
