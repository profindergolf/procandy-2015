class AddLatLng < ActiveRecord::Migration
  def change
  	add_column :profiles, :latitude, :float
  	add_column :profiles, :longitude, :float
  	remove_column :profiles, :email, :string
  end
end
