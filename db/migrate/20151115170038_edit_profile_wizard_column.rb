class EditProfileWizardColumn < ActiveRecord::Migration
  def change
  	change_column :profiles, :wizard, :boolean, :default => true
  end
end
