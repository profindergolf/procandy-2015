class AddProAvatar < ActiveRecord::Migration
  def change
  	create_table :images do |t|
      t.string :image,             default: "", null: false
	    t.integer :user_id,          default: 0, null: false
	    t.string :type, 			       default: "image", null: false
      t.datetime :created
      t.datetime :updated

      t.timestamps null: false
    end

    add_index :images, :id,         unique: true
    add_index :images, :user_id,    unique: true
  end
end
