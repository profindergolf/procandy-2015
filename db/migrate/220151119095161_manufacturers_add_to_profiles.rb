class ManufacturersAddToProfiles < ActiveRecord::Migration
  def change
    create_table :profilebrands do |t|
    	t.integer :profile_id, 	default:0, null:false
    	t.integer :brand_id, 			default:0, null:false

    	t.timestamps null: false
    end
  end
end
