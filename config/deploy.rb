# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'pro-finder-golf'
set :pty, true
set :repo_url, 'git@bitbucket.org:profindergolf/procandy-2015.git'

set :deploy_user, 'codeship'

set :assets_roles, [:app]

# setup rvm.
set :rbenv_type, :user
set :rbenv_ruby, '2.3.0'
set :rbenv_prefix, "RBENV_ROOT=/home/codeship/.rbenv RBENV_VERSION=2.3.0 /home/codeship/.rbenv/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}

set :linked_files, %w{config/database.yml config/application.yml config/secrets.yml}

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  before :deploy, "deploy:check_revision"
  # compile assets locally then rsync

  #after 'deploy:symlink:shared', 'deploy:compile_assets_locally'

  after "deploy:updated", "newrelic:notice_deployment"

  after :finishing, 'deploy:cleanup'

  after :finishing, 'deploy:update_cron'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  before :updating, "deploy:pause_puma"

  after :deploy, 'deploy:symlinks'

  after :deploy, 'deploy:start_puma'

  #after :deploy, 'deploy:puma_nr'

end
