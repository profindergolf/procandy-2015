set :output, "#{path}/log/cron.log"
set :environment, "production"

#every 1.day, :at => '0:00 am' do
	#runner "Classifieds.ad_date_check"
#end

every 10.minutes do
	runner "Classifieds.ad_date_check"
end

every '0 0 1 * *' do
	runner "Plans.top_up_credits"
end