Rails.application.routes.draw do
  resources :profiles
  resources :classifieds
  devise_for :users, controllers: { registrations: "registrations", :omniauth_callbacks => "omniauth_callbacks"  }, path_names: { sign_up: ''}
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root 'pages#home'
  
   %w( 404 422 500 503 ).each do |code|
    get code, :to => "errors#show", :code => code
  end

  get 'my-profile', to: 'profiles#profileoverview'

  get 'dashboard', to: 'dashboard#index'

  get 'signup', to: 'pages#signup'

  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    delete 'logout', to: 'devise/sessions#destroy'
    get 'logout', to: 'devise/sessions#destroy'
  end

  get 'search', to: 'search#search'
  get 'search/autocomplete', to: 'search#autocomplete'

  post 'classifieds/product/confirm', to: 'classifieds#confirmPurchase'

  get 'users/upgrade', to: 'subscription#upgrade'
  get 'users/subscribe/confirm', to: 'subscription#confirm'
  get 'users/subscribe/success', to: 'subscription#success'
  get 'users/subscribe/error', to: 'subscription#error'
  post 'users/subscribe/', to: 'subscription#subscribe'
  post 'classifieds/payment/confirmation/:id', to: 'classifieds#stripeProcess'
  get 'classifieds/addons/confirm', to: 'classifieds#addonConfirm'
  get 'profile/gallery', to: 'profiles#galleryOverview'
  get 'invoices', to: 'classifieds#soldInvoices'
  get 'invoices/view/:id', to: 'classifieds#viewInvoice'
  put 'invoices/refund/:id', to: 'classifieds#processRefund'
  get 'purchase/:id/thankyou', to: 'classifieds#thankyou'

  post 'classifieds/:id/addons', to: 'classifieds#processAddon'
  get 'update_cats', to: 'classifieds#update_cats',  :defaults => { :format => 'json' }
  get 'ebaygetitem', to: 'classifieds#getebayitem',  :defaults => { :format => 'json' }
  get 'gbgetitem', to: 'classifieds#getgbitem',  :defaults => { :format => 'json' }
  get 'changeArchive', to: 'classifieds#changeArchive', :defaults => { :format => 'json' }
  get 'smartSearch', to: 'search#ajaxSearch', :defaults => { :format => 'json' }
  post 'submithelp', to: 'application#create_groove_ticket', :defaults => { :format => 'json' }
  post 'addonCreditsProcess', to: 'classifieds#processAddonCredits'
  post 'getBlogPosts', to: 'pages#getBlogPost',  :defaults => { :format => 'json' }
  get 'profile/:userid/gallery/:imgid/delete', to: 'profiles#galleryImageDelete', :defaults => {:format => 'json'}
  post 'profile/:id/gallery/new', to: 'profiles#galleryImageUpload', :defaults => {:format => 'json'}
  delete 'classifieds/:id/images/:imgid', to: 'classifieds#extraImageDelete', :defaults => {:format => 'json'}
  post 'profile/:id/status', to: 'profiles#createNewStatus', :defaults => {:format => 'json'}
  delete 'profile/status/:statusid', to: 'profiles#deleteStatus', :defaults => {:format => 'json'}
  post 'users/:userid/subscribe/cardchange',  to: 'subscription#cardChange', :defaults => {:format => 'json'}
  post 'profiles/:id/contact', to: 'profiles#emailContact', :defaults => {:format => 'json'}
  #get 'classifieds/update_cats', to: 'classifieds#update_cats',  :defaults => { :format => 'json' }

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
